//
//  UICollectionViewCell.cpp
//  Empty
//
//  Created by Mikhail Shulepov on 10.11.15.
//
//

#include "UICollectionViewCell.hpp"
#include "ui/UIHelper.h"

NS_CC_BEGIN

namespace ui {
    void CollectionReusableView::prepareForReuse() {

    }
    
    void CollectionReusableView::flatten() {
        std::function<void(Node *, int &)> recursiveChildrenIterate = [&recursiveChildrenIterate](Node *node, int &order) {
            for (auto &&child: node->getChildren()) {
                child->setGlobalZOrder(order);
                order += 1;
                recursiveChildrenIterate(child, order);
            }
        };
        int globalZOrder = 1;
        recursiveChildrenIterate(this, globalZOrder);
    }
    
    void CollectionReusableView::applyLayoutAttributes(const CollectionViewLayoutAttributes &layoutAttributes) {
        if (_layoutAttributes != layoutAttributes) {
            _layoutAttributes = layoutAttributes;
            setAnchorPoint(Vec2(0.5f, 0.5f));
            setPosition(layoutAttributes.getCenter());
            setVisible(layoutAttributes.isVisible());
            setOpacity(layoutAttributes.getOpacity());
            setContentSize(layoutAttributes.getSize());
            setSkewX(layoutAttributes.getSkew().x);
            setSkewY(layoutAttributes.getSkew().y);
            ui::Helper::doLayout(this);
        }
    }
    
    void CollectionViewCell::prepareForReuse() {
        CollectionReusableView::prepareForReuse();
        setSelected(false, false);
        setHighlighted(false, false);
    }
    
    }
}

NS_CC_END