//
//  UICollectionViewUpdateItem.cpp
//  Empty
//
//  Created by Mikhail Shulepov on 11.11.15.
//
//

#include "UICollectionViewUpdateItem.hpp"

NS_CC_BEGIN

namespace ui {
    CollectionViewUpdateItem::CollectionViewUpdateItem(const IndexPath &initialIndexPath, const IndexPath &finalIndexPath, CollectionUpdateAction action)
    : _indexPathBeforeUpdate(initialIndexPath)
    , _indexPathAfterUpdate(finalIndexPath)
    , _updateAction(action) {
        
    }
    
    
    CollectionViewUpdateItem CollectionViewUpdateItem::create(CollectionUpdateAction action, const IndexPath &indexPath) {
        if (action == CollectionUpdateAction::INSERT) {
            return CollectionViewUpdateItem(IndexPath::NONE, indexPath, action);
        } else if (action == CollectionUpdateAction::DELETE) {
            return CollectionViewUpdateItem(indexPath, IndexPath::NONE, action);
        } else if (action == CollectionUpdateAction::RELOAD || action == CollectionUpdateAction::NONE) {
            return CollectionViewUpdateItem(indexPath, indexPath, action);
        }
        
        throw "For action move should be used default constructor or createMoveUpdate";
    }
    
    CollectionViewUpdateItem CollectionViewUpdateItem::createMoveUpdate(const IndexPath &oldIndexPath, const IndexPath &newIndexPath) {
        return CollectionViewUpdateItem(oldIndexPath, newIndexPath, CollectionUpdateAction::MOVE);
    }
    
    bool CollectionViewUpdateItem::isSectionOperation() const {
        return !_indexPathBeforeUpdate.isItem() || !_indexPathAfterUpdate.isItem();
    }
    
    int CollectionViewUpdateItem::compareIndexPaths(const CollectionViewUpdateItem &otherItem) const {
        IndexPath selfIndexPath = IndexPath::NONE;
        IndexPath otherIndexPath = IndexPath::NONE;
        
        switch (_updateAction) {
            case CollectionUpdateAction::INSERT: {
                selfIndexPath = _indexPathAfterUpdate;
                otherIndexPath = otherItem.getIndexPathAfterUpdate();
            }
                break;
            case CollectionUpdateAction::DELETE: {
                selfIndexPath = _indexPathBeforeUpdate;
                otherIndexPath = otherItem.getIndexPathBeforeUpdate();
            }
                break;
            default:
                break;
        }

        if (selfIndexPath.getSection() > otherIndexPath.getSection()) {
            return 1;
        } else if (selfIndexPath.getSection() < otherIndexPath.getSection()) {
            return -1;
        }
        
        if (isSectionOperation()) {
            return 0;
        } else  if (selfIndexPath.getItem() > otherIndexPath.getItem()) {
            return 1;
        } else if (selfIndexPath.getItem() < otherIndexPath.getItem()) {
            return -1;
        }
        return 0;
    }
    
    int CollectionViewUpdateItem::inverseCompareIndexPaths(const CollectionViewUpdateItem &otherItem) const {
        return compareIndexPaths(otherItem) * -1;
    }
}




NS_CC_END