//
//  UICollectionView_private.h
//  Empty
//
//  Created by Mikhail Shulepov on 19.11.15.
//
//

#pragma once

#include "base/CCRef.h"
#include "UICollectionViewData.hpp"
#include "IndexPath.h"

NS_CC_BEGIN

namespace ui {
    struct CollectionViewUpdateInfo {
        RefPtr<CollectionViewData> oldModel;
        RefPtr<CollectionViewData> newModel;
        std::vector<Index> oldToNewIndexMap;
        std::vector<Index> newToOldIndexMap;
    };

}

NS_CC_END

