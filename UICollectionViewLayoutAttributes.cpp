//
//  UICollectionViewLayoutAttributes.cpp
//  Empty
//
//  Created by Mikhail Shulepov on 21.11.15.
//
//

#include "UICollectionViewLayoutAttributes.hpp"

NS_CC_BEGIN

namespace ui {
 
    bool operator ==(const CollectionViewLayoutAttributes &lhs, const CollectionViewLayoutAttributes &rhs) {
        return lhs._representedElementCategory == rhs._representedElementCategory
        && lhs._indexPath == rhs._indexPath
        && lhs._center.equals(rhs._center)
        && lhs._size.equals(rhs._size)
        && lhs._opacity == rhs._opacity
        && lhs._zIndex == rhs._zIndex
        && lhs._visible == rhs._visible
        && lhs._skew.equals(rhs._skew)
        && lhs._representedElementKind == rhs._representedElementKind;
    }
    
    bool operator !=(const CollectionViewLayoutAttributes &lhs, const CollectionViewLayoutAttributes &rhs) {
        return !(lhs == rhs);
    }
    
    CollectionViewLayoutAttributes::CollectionViewLayoutAttributes()
    : _opacity(255)
    , _visible(true)
    , _zIndex(0)
    , _skew(Vec2::ZERO) {
        
    }
    
    CollectionViewLayoutAttributes::CollectionViewLayoutAttributes(const IndexPath &indexPath)
    : CollectionViewLayoutAttributes() {
        _indexPath = indexPath;
        _representedElementCategory = CollectionViewItemType::CELL;
    }
    
    CollectionViewLayoutAttributes::CollectionViewLayoutAttributes(const std::string &elementKind, Index section)
    : CollectionViewLayoutAttributes() {
        _indexPath = IndexPath(section, 0);
        _representedElementCategory = CollectionViewItemType::SUPPLEMENTARY_VIEW;
        _representedElementKind = elementKind;
    }
    
    void CollectionViewLayoutAttributes::setFrame(const Rect &frame) {
        _size = frame.size;
        _center = Point(frame.getMidX(), frame.getMidY());
    }
    
    void CollectionViewLayoutAttributes::setCenter(const Point &center) {
        _center = center;
    }
    
    void CollectionViewLayoutAttributes::setSize(const Point &size) {
        _size = size;
    }
    
    Rect CollectionViewLayoutAttributes::getFrame() const {
        return Rect(_center.x - _size.width / 2, _center.y - _size.height / 2, _size.width, _size.height);
    }
}

NS_CC_END