//
//  UICollectionViewFlowLayout.hpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 16.12.15.
//
//

#pragma once

#include <unordered_map>

#include "UICollectionViewLayout.hpp"
#include "UICollectionViewCommon.h"

NS_CC_BEGIN

namespace ui {
    class CollectionViewFlowLayoutDelegate;
    class GridLayoutInfo;
    
    class CollectionViewFlowLayout: public CollectionViewLayout {
    public:
        CC_SYNTHESIZE(float, _minimumLineSpacing, MinimumLineSpacing);
        CC_SYNTHESIZE(float, _minimumInterItemSpacing, MinimumInterItemSpacing);
        CC_SYNTHESIZE(Size, _itemSize, ItemSize); // for the cases the delegate method is not implemented
        CC_SYNTHESIZE(Size, _headerReferenceSize, HeaderReferenceSize);
        CC_SYNTHESIZE(Size, _footerReferenceSize, FooterReferenceSize);
        CC_SYNTHESIZE(Margin, _sectionInset, SectionInset);
        CC_SYNTHESIZE(LayoutAlignmentOptions, _alignment, Alignment);
        CC_SYNTHESIZE(bool, _stickyHeader, StickyHeader);
        CC_SYNTHESIZE(bool, _forceVerticalStyle, ForceVerticalStyle);
               
        CollectionViewFlowLayout();
        
        virtual void invalidateLayout() override;
        virtual void prepareLayout() override;
        
        virtual std::vector<CollectionViewLayoutAttributes> getLayoutAttributesForElementsInRect(const Rect &rect) override;
        
        virtual CollectionViewLayoutAttributes getLayoutAttributesForItem(const IndexPath &indexPath) override;
        virtual std::shared_ptr<CollectionViewLayoutAttributes> getLayoutAttributesForSupplementaryView(const std::string &kind, const IndexPath &indexPath) override;

        virtual Size getCollectionViewContentSize() override;
        
        virtual bool shouldInvalidateLayoutForBoundsChange(const Rect &newBounds) const override;
        virtual Vec2 getTargetContentOffsetForProposedContentOffset(const Vec2 &proposedContentOffset, const Vec2 &scrollingVelocity) const override;
        
    private:
        void fetchItemsInfo();
        void getSizingInfos();
        void updateItemsLayout();
        
    private:
        GridLayoutInfo *_data;
        std::unordered_map<Index, std::vector<Rect>> _rectsCache; //<section, [item rects]>
        mutable Size _collectionViewSize;
    };
    
    class CollectionViewFlowLayoutDelegate {
    public:
        virtual Size getSizeForItem(CollectionView *cv, CollectionViewFlowLayout *layout, const IndexPath &indexPath) const {
            return Size(-1, -1);
        }
        
        virtual Margin getInsetForSection(CollectionView *cv, CollectionViewFlowLayout *layout, Index section) const {
            return layout->getSectionInset();
        }
        
        virtual float getMinimumLineSpacingForSection(CollectionView *cv, CollectionViewFlowLayout *layout, Index section) const {
            return layout->getMinimumLineSpacing();
        }
        
        virtual float getMinimumInterItemSpacingForSection(CollectionView *cv, CollectionViewFlowLayout *layout, Index section) const {
            return layout->getMinimumInterItemSpacing();
        }
        
        virtual Size getReferenceSizeForHeaderInSection(CollectionView *cv, CollectionViewFlowLayout *layout, Index section) const {
            return layout->getHeaderReferenceSize();
        }
        
        virtual Size getReferenceSizeForFooterInSection(CollectionView *cv, CollectionViewFlowLayout *layout, Index section) const {
            return layout->getFooterReferenceSize();
        }
    };
}

NS_CC_END