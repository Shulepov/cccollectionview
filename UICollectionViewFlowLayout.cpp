//
//  UICollectionViewFlowLayout.cpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 16.12.15.
//
//

#include "UICollectionViewFlowLayout.hpp"
#include "UICollectionView.hpp"

#include "UIGridLayoutInfo.hpp"
#include "UIGridLayoutSection.hpp"
#include "UIGridLayoutRow.hpp"
#include "UIGridLayoutItem.hpp"

NS_CC_BEGIN

namespace ui {   
    CollectionViewFlowLayout::CollectionViewFlowLayout() {
        _data = nullptr;
        _itemSize = Size(50.0f, 50.0f);
        _minimumLineSpacing = 10.0f;
        _minimumInterItemSpacing = 10.0f;
        _sectionInset = Margin::ZERO;
        _headerReferenceSize = Size::ZERO;
        _footerReferenceSize = Size::ZERO;
        _stickyHeader = false;
        _forceVerticalStyle = false;
    }
    
    Vec2 CollectionViewFlowLayout::getTargetContentOffsetForProposedContentOffset(const Vec2 &proposedContentOffset, const Vec2 &scrollingVelocity) const {
        return proposedContentOffset;
    }
    
    std::vector<CollectionViewLayoutAttributes> CollectionViewFlowLayout::getLayoutAttributesForElementsInRect(const Rect &rect) {
        if (!_data) {
            prepareLayout();
        }
        
        const bool isHorizontal = getCollectionView()->getDirection() != ScrollView::Direction::VERTICAL;
        std::vector<CollectionViewLayoutAttributes> layoutAttributesArray;
        const auto &sections = _data->getSections();
        for (Index sectionIndex = 0; sectionIndex < sections.size(); ++sectionIndex) {
            GridLayoutSection *section = sections.at(sectionIndex);
            const Rect sectionFrame = section->getFrame();
            if (rect.intersectsRect(sectionFrame)) {

                // if we have fixed size, calculate item frames only once.
                // this also uses the default FlowLayoutCommonRowHorizontalAlignmentKey alignment
                // for the last row. (we want this effect!)
                // we should firstly calculate footer height to be able to adjust positions of sticky headers
                Rect normalizedFooterFrame = section->getFooterFrame();
                normalizedFooterFrame.origin.x += sectionFrame.origin.x;
                normalizedFooterFrame.origin.y = sectionFrame.origin.y;
                
                Rect normalizedHeaderFrame = section->getHeaderFrame();
                normalizedHeaderFrame.origin.x += sectionFrame.origin.x;
                if (_stickyHeader && !isHorizontal && rect.getMaxY() < sectionFrame.getMaxY()) {
                    normalizedHeaderFrame.origin.y = std::max(rect.getMaxY() - normalizedHeaderFrame.size.height, normalizedFooterFrame.getMaxY());
                } else if (_stickyHeader && isHorizontal && rect.getMinX() > sectionFrame.getMinX()) {
                    normalizedHeaderFrame.origin.x = std::min(rect.getMinX(), normalizedFooterFrame.getMinX() - normalizedHeaderFrame.size.width);
                } else {
                    normalizedHeaderFrame.origin.y = sectionFrame.getMaxY() - normalizedHeaderFrame.getMaxY();
                }
                
                if (!normalizedHeaderFrame.size.equals(Size::ZERO) && rect.intersectsRect(normalizedHeaderFrame)) {
                    CollectionViewLayoutAttributes attrs(CollectionElementKindSectionHeader, sectionIndex);
                    attrs.setFrame(normalizedHeaderFrame);
                    layoutAttributesArray.push_back(attrs);
                }
                
                const auto &rows = section->getRows();
                
                const bool isFixedItemSize = section->getFixedItemSize();
                auto itemRectsIt = _rectsCache.find(sectionIndex);
                std::vector<Rect> itemRects;
                if (itemRectsIt == _rectsCache.end()) {
                    if (isFixedItemSize && !rows.empty()) {
                        itemRects = rows.at(0)->getItemRects();
                        if (!itemRects.empty()) {
                            _rectsCache[sectionIndex] = itemRects;
                        }
                    }
                } else {
                    itemRects = (*itemRectsIt).second;;
                }

                for (GridLayoutRow *row: rows) {
                    Rect normalizedRowFrame = row->getRowFrame();
                    normalizedRowFrame.origin.x += sectionFrame.origin.x;
                    normalizedRowFrame.origin.y = sectionFrame.getMaxY() - normalizedRowFrame.getMaxY();
                    
                    if (rect.intersectsRect(normalizedRowFrame)) {
                        // TODO be more fine-grained for items
                        for (Index itemIndex = 0; itemIndex < row->getItemCount(); ++itemIndex) {
                            Index sectionItemIndex;
                            Rect itemFrame;
                            if (isFixedItemSize) {
                                itemFrame = itemRects[itemIndex];
                                sectionItemIndex = row->getIndex() * section->getItemsByRowCount() + itemIndex;
                            } else {
                                GridLayoutItem *item = row->getItems().at(itemIndex);
                                sectionItemIndex = section->getIndexOfItem(item);
                                itemFrame = item->getFrame();
                            }
                            
                            Rect normalizedItemFrame(normalizedRowFrame.origin + itemFrame.origin, itemFrame.size);
                            
                            if (rect.intersectsRect(normalizedItemFrame)) {
                                CollectionViewLayoutAttributes attrs(IndexPath(sectionIndex, sectionItemIndex));
                                attrs.setFrame(normalizedItemFrame);
                                layoutAttributesArray.push_back(attrs);
                            }
                        }
                    }
                }
                

                if (!normalizedFooterFrame.size.equals(Size::ZERO) && rect.intersectsRect(normalizedFooterFrame) ) {
                    CollectionViewLayoutAttributes attrs(CollectionElementKindSectionFooter, sectionIndex);
                    attrs.setFrame(normalizedFooterFrame);
                    layoutAttributesArray.push_back(attrs);
                }
            }
        }
        
        return layoutAttributesArray;
    }
    
    CollectionViewLayoutAttributes CollectionViewFlowLayout::getLayoutAttributesForItem(const IndexPath &indexPath) {
        if (!_data) {
            prepareLayout();
        }
        
        GridLayoutSection *section = _data->getSections().at(indexPath.getSection());
        GridLayoutRow *row = nullptr;
        Rect itemFrame = Rect::ZERO;
        
        const ssize_t itemsByRowCount = section->getItemsByRowCount();
        const Index rowIndex = indexPath.getItem() / itemsByRowCount;
        if (section->getFixedItemSize() && itemsByRowCount > 0 && rowIndex < section->getRowsCount() ) {
            row = section->getRows().at(rowIndex);
            const Index itemIndex = indexPath.getItem() % itemsByRowCount;
            itemFrame = row->getItemRects()[itemIndex];
            
        } else if (indexPath.getItem() < section->getItemsCount()) {
            GridLayoutItem *item = section->getItem(indexPath.getItem());
            row = item->getRow();
            itemFrame = item->getFrame();
        }
        
        CollectionViewLayoutAttributes attrs = CollectionViewLayoutAttributes(indexPath);
        
        // calculate item rect
        const Rect sectionFrame = section->getFrame();
        Rect normalizedRowFrame = row->getRowFrame();
        normalizedRowFrame.origin.x += sectionFrame.origin.x;
        normalizedRowFrame.origin.y = sectionFrame.getMaxY() - normalizedRowFrame.getMaxY();
        attrs.setFrame(Rect(normalizedRowFrame.origin + itemFrame.origin, itemFrame.size));

        return attrs;
    }
    
    std::shared_ptr<CollectionViewLayoutAttributes> CollectionViewFlowLayout::getLayoutAttributesForSupplementaryView(const std::string &kind, const IndexPath &indexPath) {
        if (!_data) {
            prepareLayout();
        }
        
        if (indexPath.getSection() < _data->getSections().size()) {
            GridLayoutSection *section = _data->getSections().at(indexPath.getSection());
            
            Rect normalizedFrame = Rect::ZERO;
            if (kind == CollectionElementKindSectionHeader) {
                normalizedFrame = section->getHeaderFrame();
            } else if (kind == CollectionElementKindSectionFooter) {
                normalizedFrame = section->getFooterFrame();
            }
            
            if (!normalizedFrame.size.equals(Size::ZERO)) {
                const Rect sectionFrame = section->getFrame();
                normalizedFrame.origin.x += sectionFrame.origin.x;
                normalizedFrame.origin.y = sectionFrame.getMaxY() - normalizedFrame.getMaxY();
                
                CollectionViewLayoutAttributes attrs = CollectionViewLayoutAttributes(kind, indexPath.getSection());
                attrs.setFrame(normalizedFrame);
                return std::make_shared<CollectionViewLayoutAttributes>(attrs);
            }
        }
        
        return nullptr;
    }
    
    Size CollectionViewFlowLayout::getCollectionViewContentSize() {
        if (!_data) {
            prepareLayout();
        }
        
        return _data->getContentSize();
    }
    
    void CollectionViewFlowLayout::invalidateLayout() {
        CollectionViewLayout::invalidateLayout();
        _rectsCache.clear();
        CC_SAFE_RELEASE_NULL(_data);
    }
    
    void CollectionViewFlowLayout::prepareLayout() {
        _rectsCache.clear();
        _data = new GridLayoutInfo();
        _data->setHorizontal(!_forceVerticalStyle && getCollectionView()->getDirection() == ScrollView::Direction::HORIZONTAL);
        
        _collectionViewSize = getCollectionView()->getContentSize();
        _data->setDimension(_data->getHorizontal() ? _collectionViewSize.height : _collectionViewSize.width);
        _data->setAlignmentOptions(_alignment);
        
        fetchItemsInfo();
    }
    
    bool CollectionViewFlowLayout::shouldInvalidateLayoutForBoundsChange(const Rect &newBounds) const {
        const bool isHorizontal = getCollectionView()->getDirection() == ScrollView::Direction::HORIZONTAL;
        if ( (isHorizontal && _collectionViewSize.height != newBounds.size.height) || (!isHorizontal && _collectionViewSize.width != newBounds.size.width)) {
            _collectionViewSize = newBounds.size;
            return true;
        }
        return false;
    }
    
#pragma mark - Private
    
    void CollectionViewFlowLayout::fetchItemsInfo() {
        getSizingInfos();
        updateItemsLayout();
    }
    
    void CollectionViewFlowLayout::getSizingInfos() {
        CollectionView *cv = getCollectionView();
        CollectionViewFlowLayoutDelegate *delegate = dynamic_cast<CollectionViewFlowLayoutDelegate *>(cv->getDelegate());
        
        const ssize_t numberOfSection = getCollectionView()->getNumberOfSections();
        const bool isHorizontal = _data->getHorizontal();
        for (Index section = 0; section < numberOfSection; ++section) {
            GridLayoutSection *layoutSection = _data->addSection();
            
            Margin sectionInset(_sectionInset);
            float minimumInterItemSpacing = _minimumInterItemSpacing;
            float minimumLineSpacing = _minimumLineSpacing;
            Size headerReferenceSize(_headerReferenceSize);
            Size footerReferenceSize(_footerReferenceSize);
            
            if (delegate) {
                sectionInset = delegate->getInsetForSection(cv, this, section);
                minimumInterItemSpacing = delegate->getMinimumInterItemSpacingForSection(cv, this, section);
                minimumLineSpacing = delegate->getMinimumLineSpacingForSection(cv, this, section);
                headerReferenceSize = delegate->getReferenceSizeForHeaderInSection(cv, this, section);
                footerReferenceSize = delegate->getReferenceSizeForFooterInSection(cv, this, section);
            }
            
            layoutSection->setSectionMargins(sectionInset);
            if (isHorizontal) {
                layoutSection->setVerticalInterstice(minimumInterItemSpacing);
                layoutSection->setHorizontalInterstice(minimumLineSpacing);
                layoutSection->setHeaderDimension(headerReferenceSize.width);
                layoutSection->setFooterDimension(footerReferenceSize.width);
            } else {
                layoutSection->setVerticalInterstice(minimumLineSpacing);
                layoutSection->setHorizontalInterstice(minimumInterItemSpacing);
                layoutSection->setHeaderDimension(headerReferenceSize.height);
                layoutSection->setFooterDimension(footerReferenceSize.height);
            }
            
            const ssize_t numberOfItems = cv->getNumberOfItemsInSection(section);

            //check if delegate implements size delegate
            bool fixedItemsSize = true;
            if (delegate) {
                const Size delegateItemSize = delegate->getSizeForItem(cv, this, IndexPath(section, 0));
                fixedItemsSize = delegateItemSize.width < 0;
            }
            
            if (fixedItemsSize) {
                layoutSection->setFixedItemSize(true);
                layoutSection->setItemSize(_itemSize);
                layoutSection->setItemsCount(numberOfItems);
            } else {               
                for (Index item = 0; item < numberOfItems; ++item) {
                    const IndexPath indexPath(section, item);
                    const Size itemSize = delegate->getSizeForItem(cv, this, indexPath);
                    GridLayoutItem *layoutItem = layoutSection->addItem();
                    layoutItem->setFrame(Rect(Point::ZERO, itemSize));
                }
            }
        }
    }
    
    void CollectionViewFlowLayout::updateItemsLayout() {
        Size contentSize = Size::ZERO;
        const bool isHorizontal = getCollectionView()->getDirection() != ScrollView::Direction::VERTICAL;
        const Size collectionViewSize = getCollectionView()->getContentSize();
        for (GridLayoutSection *section: _data->getSections()) {
            section->computeLayout();
            
            // update section offset to make frame absolute (section only calculates relative)
            Rect sectionFrame = section->getFrame();
            if (isHorizontal) {
                const float extraContentSize = sectionFrame.size.width + sectionFrame.origin.x;
                sectionFrame.origin.x += contentSize.width;
                contentSize.width += extraContentSize;
                contentSize.height = std::max(contentSize.height, sectionFrame.size.height + sectionFrame.origin.y);
            } else {
                const float extraContentSize = sectionFrame.size.height + sectionFrame.origin.y;
                sectionFrame.origin.y += contentSize.height;
                contentSize.height += extraContentSize;
                contentSize.width = std::max(contentSize.width, sectionFrame.size.width + sectionFrame.origin.x);
            }
            section->setFrame(sectionFrame);
        }
        
        if (isHorizontal != _data->getHorizontal() && isHorizontal) {
            //if Vertical style presented in horizontal scrolling
            //we want to align sections by header
            const float totalHeight = std::max(getCollectionView()->getContentSize().height, contentSize.height);
            for (GridLayoutSection *section: _data->getSections()) {
                Rect sectionFrame = section->getFrame();
                sectionFrame.origin.y = totalHeight - sectionFrame.size.height;
                section->setFrame(sectionFrame);
            }
        } else if (!isHorizontal) {
            //first sections should have greater y position
            for (GridLayoutSection *section: _data->getSections()) {
                Rect sectionFrame = section->getFrame();
                sectionFrame.origin.y = contentSize.height - sectionFrame.origin.y - sectionFrame.size.height;
                section->setFrame(sectionFrame);
            }
        }
               
        _data->setContentSize(contentSize);
    }
}

NS_CC_END