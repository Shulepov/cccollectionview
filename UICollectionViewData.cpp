//
//  UICollectionViewData.cpp
//  Empty
//
//  Created by Mikhail Shulepov on 11.11.15.
//
//

#include "UICollectionViewData.hpp"
#include "UICollectionViewLayout.hpp"
#include "UICollectionView.hpp"

NS_CC_BEGIN

namespace ui {
    
    CollectionViewData::CollectionViewData(CollectionView *collectionView, CollectionViewLayout *layout)
    : _collectionView(collectionView)
    , _layout(layout) {
        
    }
    
#pragma mark - Public
    void CollectionViewData::invalidate() {
        _collectionViewDataFlags.itemCountsAreValid = 0;
        _collectionViewDataFlags.layoutIsPrepared = 0;
        _validLayoutRect = Rect::ZERO;
    }
    
    void CollectionViewData::validateLayoutInRect(const cocos2d::Rect &rect) {
        validateItemsCount();
        prepareToLoadData();
        
        if (!_validLayoutRect.equals(rect)) {
            _validLayoutRect = rect;
            _cachedLayoutAttributes = _layout->getLayoutAttributesForElementsInRect(rect);
        }
    }
    
    Index CollectionViewData::getGlobalIndexForItemAtIndexPath(const IndexPath &indexPath) {
        return getNumberOfItemsBeforeSection(indexPath.getSection()) + indexPath.getItem();
    }
    
    IndexPath CollectionViewData::getIndexPathForItemAtGlobalIndex(Index index) {
        validateItemsCount();
        
//       CCASSERT(index < _numItems, @"request for index path for global index %ld when there are only %ld items in the collection view", (long)index, (long)_numItems);
        
        Index section = 0;
        ssize_t countItems = 0;
        for (section = 0; section < _numSections; ++section) {
            const ssize_t countIncludingThisSection = countItems + _sectionItemCounts[section];
            if (countIncludingThisSection > index) {
                break;
            }
            countItems = countIncludingThisSection;
        }
        Index item = index - countItems;
        return IndexPath(section, item);
    }
    
    std::vector<CollectionViewLayoutAttributes> CollectionViewData::getLayoutAttributesForElementsInRect(const Rect &rect) {
        validateLayoutInRect(rect);
        return _cachedLayoutAttributes;
    }
    
    
#pragma mark - Private
    
    // ensure item count is valid and loaded
    void CollectionViewData::validateItemsCount() {
        if (!_collectionViewDataFlags.itemCountsAreValid) {
            updateItemsCount();
        }
    }
    
    void CollectionViewData::prepareToLoadData() {
        if (!_collectionViewDataFlags.layoutIsPrepared) {
            _layout->prepareLayout();
            _contentSize = _layout->getCollectionViewContentSize();
            _collectionViewDataFlags.layoutIsPrepared = 1;
        }
    }
    
    ssize_t CollectionViewData::getNumberOfItemsBeforeSection(Index section) {
        validateItemsCount();
        
//      NSAssert(section < _numSections, @"request for number of items in section %ld when there are only %ld sections in the collection view", (long)section, (long)_numSections);
        
        ssize_t returnCount = 0;
        for (Index i = 0; i < section; ++i) {
            returnCount += _sectionItemCounts[i];
        }
        return returnCount;
    }
    
    // query dataSource for new data
    void CollectionViewData::updateItemsCount() {
        auto dataSource = _collectionView->getDataSource();
        
        // query how many sections there will be
        _numSections = dataSource->getNumberOfSections(_collectionView);
        _numItems = 0;
        if (_numSections == 0) { //early bail-out
            _sectionItemCounts.clear();
            _collectionViewDataFlags.itemCountsAreValid = 1;
            return;
        }
        
        //query cells per section
        _sectionItemCounts.resize(_numSections);
        for (Index i = 0; i < _numSections; ++i) {
            const ssize_t cellCount = dataSource->getNumberOfItemsInSection(_collectionView, i);
            _sectionItemCounts[i] = cellCount;
            _numItems += cellCount;
        }
        
        _collectionViewDataFlags.itemCountsAreValid = 1;
    }
    
}
NS_CC_END