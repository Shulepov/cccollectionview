//
//  UICollectionViewItemKey.hpp
//  Empty
//
//  Created by Mikhail Shulepov on 12.11.15.
//
//

#pragma once

#include <functional>

#include "base/CCRef.h"
#include "IndexPath.h"
#include "UICollectionViewLayoutAttributes.hpp"

NS_CC_BEGIN

namespace ui {
    class CollectionViewItemKey {
    public:
        CollectionViewItemKey(const CollectionViewItemKey &other);
        static CollectionViewItemKey make(const CollectionViewLayoutAttributes &layoutAttributes);
        static CollectionViewItemKey make(const IndexPath &indexPath);
        static CollectionViewItemKey makeForSupplementaryView(const IndexPath &indexPath, const std::string &elementKind);
        
        CollectionViewItemType getType() const {
            return _type;
        }
        
        const IndexPath &getIndexPath() const {
            return _indexPath;
        }
        
        std::size_t getHash() const {
            return _hash;
        }
        
        bool operator ==(const CollectionViewItemKey &b) const;
        bool operator <(const CollectionViewItemKey &b) const;
        
    private:
        CollectionViewItemKey() {}
        static CollectionViewItemKey make(const IndexPath &indexPath, CollectionViewItemType type, const std::string &identifier);
        
        CollectionViewItemType _type;
        IndexPath _indexPath;
        size_t _hash;
    };
}

NS_CC_END

namespace std {
    template <>
    struct hash<cocos2d::ui::CollectionViewItemKey> {
        std::size_t operator()(const cocos2d::ui::CollectionViewItemKey &k) const {
            return k.getHash();
        }
    };    
}