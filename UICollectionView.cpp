//
//  UICollectionView.cpp
//  Empty
//
//  Created by Mikhail Shulepov on 10.11.15.
//
//

#include <unordered_set>
#include <algorithm>

#include "UICollectionView.hpp"
#include "UICollectionViewLayout.hpp"
#include "UICollectionViewCell.hpp"

#include "cocostudio/ActionTimeline/CSLoader.h"

NS_CC_BEGIN

namespace ui {
    bool CollectionView::init(RefPtr<CollectionViewLayout> layout) {
        ScrollView::init();
        _layout = layout;
        _layout->_collectionView = this;
        _collectionViewData = CollectionViewData::create(this, layout);
        _allowSelection = true;
        _allowMultipleSelection = false;
        _dataSource = nullptr;
        _delegate = nullptr;
        _touchingIndexPath = IndexPath::NONE;
        _currentIndexPath = IndexPath::NONE;
        _pageSize = Size::ZERO;
        return true;
    }
    
    void CollectionView::setPageSize(const Size &size) {
        const bool isHorizontal = getDirection() != ScrollView::Direction::VERTICAL;
        if (isHorizontal) {
            _pageSize = Size(size.width, 0.0f);
        } else {
            _pageSize = Size(0.0f, size.height);
        }
    }
    
#pragma mark - Layout
    //TODO: - (void)layoutSubviews
    void CollectionView::doLayout() {
        ScrollView::doLayout();
        if (!_layout) {
            return;
        }
        Size contentSize = _layout->getCollectionViewContentSize();
        contentSize.width = std::max(getContentSize().width, contentSize.width);
        contentSize.height = std::max(getContentSize().height, contentSize.height);
        setInnerContainerSize(contentSize);
    }
    
    void CollectionView::onSizeChanged() {
        ScrollView::onSizeChanged();
        if (_layout) {
            invalidateLayout();
        }
        //TODO: _collectionViewFlags.fadeCellsForBoundsChange = YES;
    }
    
    void CollectionView::update(float dt) {
        ScrollView::update(dt);
        updateVisibleCells();
    }
    
    void CollectionView::startAttenuatingAutoScroll(const Vec2& deltaMove, const Vec2& initialVelocity) {
        /*if (_layout) {
         const Vec2 proposedContentOffset = - (getInnerContainerPosition() + deltaMove);
         const Vec2 targetContentOffset = -_layout->getTargetContentOffsetForProposedContentOffset(proposedContentOffset, initialVelocity);
         const Vec2 adjustedDeltaMove = targetContentOffset - getInnerContainerPosition();
         ScrollView::startAttenuatingAutoScroll(adjustedDeltaMove, initialVelocity);
         */
        if (_pageSize.width > FLT_EPSILON || _pageSize.height > FLT_EPSILON) {
            const Vec2 currentContentOffset = - getInnerContainerPosition();
            const Vec2 proposedContentOffset = currentContentOffset - deltaMove;

            const int currentPage = getPageNumberForContentOffset(currentContentOffset);
            const int proposedPage = getPageNumberForContentOffset(proposedContentOffset);
            int nextPage = clampf(proposedPage, std::max(0, currentPage - 1), currentPage + 1);
            
            //if paging enabled scroll only by one page
            Vec2 adjustedDeltaMove;
            do {
                const Vec2 targetContentOffset = _pageSize * nextPage;
                adjustedDeltaMove = currentContentOffset - targetContentOffset;
                if (adjustedDeltaMove.x > _pageSize.width || adjustedDeltaMove.y > _pageSize.height) {
                    nextPage++;
                } else if (adjustedDeltaMove.x < -_pageSize.width || adjustedDeltaMove.y < -_pageSize.height) {
                    nextPage--;
                }
            } while (std::abs(adjustedDeltaMove.x) > _pageSize.width || std::abs(adjustedDeltaMove.y) > _pageSize.height);
            
            const float time = adjustedDeltaMove.distance(Vec2::ZERO) / 900;
            ScrollView::startAutoScroll(adjustedDeltaMove, time, true);

        } else {
            ScrollView::startAttenuatingAutoScroll(deltaMove, initialVelocity);
        }
    }
    
    void CollectionView::handleReleaseLogic(Touch *touch) {
        ScrollView::handleReleaseLogic(touch);
        
        if (!_autoScrolling) {
            startAttenuatingAutoScroll(Vec2(0, 0), Vec2(100, 100));
        }
    }

    
#pragma mark - Public
    
    void CollectionView::registerReusableCell(CollectionViewCellFactoryPtr cellFactory, const std::string &reuseIdentifier) {
        _cellTemplatesDict.emplace(reuseIdentifier, cellFactory);
    }
    
    void CollectionView::registerSupplementaryView(CollectionReusableViewFactoryPtr viewFactory, const std::string &kind, const std::string &reuseIdentifier) {
        const std::string kindAndIdentifier = kind + "/" + reuseIdentifier;
        _supplementaryViewTemplatesDict.emplace(kindAndIdentifier, viewFactory);
    }
    
    CollectionViewCell *CollectionView::getDequeueReusableCell(const std::string &reuseIdentifier, const IndexPath &indexPath) {
        auto &reusableCells = _cellReuseQueues[reuseIdentifier];
        CollectionViewCell *cell = nullptr;
        if (!reusableCells.empty()) {
            cell = reusableCells.back();
            cell->retain();
            reusableCells.popBack();
            cell->autorelease();
            
        } else {
            CollectionViewCellFactoryPtr cellFactory = _cellTemplatesDict[reuseIdentifier];
            cell = cellFactory->createCell();
            cell->setCollectionView(this);
            cell->setReuseIdentifier(reuseIdentifier);
            disableTouchesRecursively(cell);
            cell->flatten();
        }
        
        auto attributes = _layout->getLayoutAttributesForItem(indexPath);
        cell->applyLayoutAttributes(attributes);
        return cell;
    }
    
    CollectionReusableView *CollectionView::getDequeueReusableSupplementaryView(const std::string &reuseIdentifier, const std::string &kind, const IndexPath &indexPath) {
        const std::string kindAndIdentifier = kind + "/" + reuseIdentifier;
        auto &reusableViews = _supplementaryViewReuseQueues[kindAndIdentifier];
        CollectionReusableView *view = nullptr;
        if (!reusableViews.empty()) {
            view = reusableViews.back();
            view->retain();
            reusableViews.popBack();
            view->autorelease();
        } else {
            CollectionReusableViewFactoryPtr viewFactory = _supplementaryViewTemplatesDict[kindAndIdentifier];
            view = viewFactory->createView();
            view->setCollectionView(this);
            view->setReuseIdentifier(reuseIdentifier);
            disableTouchesRecursively(view);
            view->flatten();
        }
        
        auto attributes = _layout->getLayoutAttributesForSupplementaryView(kind, indexPath);
        if (attributes) {
            view->applyLayoutAttributes(*attributes);
        }
        
        return view;
    }
    
    void CollectionView::reloadData() {
        if (_reloadingSuspendedCount != 0) return;
        invalidateLayout();
        
        for (auto &entry: _allVisibleViewsDict) {
            entry.second->removeFromParent();
        }
        _allVisibleViewsDict.clear();
        
        for (auto &&indexPath: _indexPathsForSelectedItems) {
            CollectionViewCell *cell = getCellForItemAtIndexPath(indexPath);
            if (cell) {
                cell->setSelected(false, false);
                cell->setHighlighted(false, false);
            }
        }
        _indexPathsForSelectedItems.clear();
        _indexPathsForHighlightedItems.clear();
        
        requestDoLayout();
    }
    
    
#pragma mark - Query
    
    Vector<CollectionReusableView *> CollectionView::getVisibleViews() const {
        Vector<CollectionReusableView *> ret;
        for (auto &&entry: _allVisibleViewsDict) {
            ret.pushBack(entry.second);
        }
        return ret;
    }
    
    CollectionViewCell *CollectionView::getCellForItemAtIndexPath(const IndexPath &indexPath) {
        for (auto &&entry: _allVisibleViewsDict) {
            auto &key = entry.first;
            if (key.getType() == CollectionViewItemType::CELL && key.getIndexPath() == indexPath) {
                return dynamic_cast<CollectionViewCell *>(entry.second);
            }
        }
        return nullptr;
    }
    
    IndexPath CollectionView::getIndexPathForItemAtPoint(const Point &point) const {
        const Rect rect(point, Size(1, 1));
        auto attributes = _layout->getLayoutAttributesForElementsInRect(rect);
        //leave only cells
        auto it = std::remove_if(attributes.begin(), attributes.end(), [](const CollectionViewLayoutAttributes &attrs) {
            return attrs.getRepresentedElementType() != CollectionViewItemType::CELL;
        });
        attributes.erase(it, attributes.end());
        if (attributes.empty()) {
            return IndexPath::NONE;
        } else {
            return attributes.back().getIndexPath();
        }
    }
    
    ssize_t CollectionView::getNumberOfSections() const {
        return _dataSource->getNumberOfSections(this);
    }
    
    ssize_t CollectionView::getNumberOfItemsInSection(Index section) const {
        return _dataSource->getNumberOfItemsInSection(this, section);
    }
    
    // Interacting with the collection view.
    void CollectionView::scrollToItemAtIndexPath(const IndexPath &indexPath, CollectionViewScrollPosition scrollPosition, bool animated) {
        if (scrollPosition == CollectionViewScrollPosition::None) {
            return;
        }
        
        doLayout();
        
        auto attrs = _layout->getLayoutAttributesForItem(indexPath);
        Vec2 scrollOffset = getScrollOffsetForItemWithFrame(attrs.getFrame(), scrollPosition);
        if (_pageSize.width > FLT_EPSILON || _pageSize.height > FLT_EPSILON) {
            const int pageNumber = getPageNumberForContentOffset(-scrollOffset);
            scrollOffset = _pageSize * (-pageNumber);
        }
        const float distance = scrollOffset.getDistance(getInnerContainerPosition());
        const float animationTime = distance / 1500;
        if (getDirection() == ScrollView::Direction::VERTICAL) {
            if (animated) {
                startAutoScrollToDestination(scrollOffset, animationTime, true);
            } else {
                startAutoScrollToDestination(scrollOffset, 0, false);
            }
        } else if (getDirection() == ScrollView::Direction::HORIZONTAL) {
            if (animated) {
                startAutoScrollToDestination(scrollOffset, animationTime, true);
            } else {
                startAutoScrollToDestination(scrollOffset, 0, false);
            }
        }
    }
    
    Vec2 CollectionView::getScrollOffsetForItemWithFrame(const Rect frame, CollectionViewScrollPosition scrollPosition) const {
        const Size containertSize = getInnerContainerSize();
        switch (scrollPosition) {
            case CollectionViewScrollPosition::Top:
                return Vec2(0, -frame.getMaxY() + getContentSize().height);

            case CollectionViewScrollPosition::Bottom:
                return Vec2(0, -frame.getMinY());

            case CollectionViewScrollPosition::CenteredVertically:
                return Vec2(0, -frame.getMidY() + getContentSize().height * 0.5f);

            case CollectionViewScrollPosition::Left:
                return Vec2(-frame.getMinX(), 0);

            case CollectionViewScrollPosition::Right:
                return Vec2(-frame.getMaxX() + getContentSize().width, 0);
                
            case CollectionViewScrollPosition::CenteredHorizontally:
                return Vec2(-frame.getMidX() + getContentSize().width * 0.5f, 0);
                
            default:
                break;
        }
        return Vec2::ZERO;
    }
    
    
#pragma mark - Touch Handling
    
    bool CollectionView::onTouchBegan(Touch *touch, Event *unusedEvent) {
        if (_touchId >= 0 && _touchId != touch->getID()) {
            return false;
        }
        
        _touchFailed = false;
        _maxTouchMoveDistance = 0.0f;
        if (ScrollView::onTouchBegan(touch, unusedEvent)) {
            const Point touchLocation = getInnerContainer()->convertToNodeSpace(touch->getLocation());
            const IndexPath itemIndex = getIndexPathForItemAtPoint(touchLocation);
            _currentIndexPath = itemIndex;
            
            if (itemIndex != IndexPath::NONE && _allowSelection && _delegate->isInteractableItemAtIndexPath(this, itemIndex)) {
                _touchingIndexPath = itemIndex;
                highlightItem(itemIndex, true, CollectionViewScrollPosition::None);
                
                if (!_allowMultipleSelection) {
                    //TODO: 1233 temporaraly deselect selected item?
                }
            } else {
                _touchingIndexPath = IndexPath::NONE;
            }
            
            return true;
        }
        return false;
    }
    
    void CollectionView::onTouchMoved(Touch *touch, Event *unusedEvent) {
        ScrollView::onTouchMoved(touch, unusedEvent);
        if (_touchingIndexPath != IndexPath::NONE) {
            _maxTouchMoveDistance = std::max(_maxTouchMoveDistance, getTouchBeganPosition().distance(touch->getLocation()));
            const Point touchLocation = getInnerContainer()->convertToNodeSpace(touch->getLocation());
            const IndexPath indexPath = getIndexPathForItemAtPoint(touchLocation);
            
            const bool touchFailed = _touchFailed || _maxTouchMoveDistance > _touchMoveThreshold;
            const bool touchJustFailed = touchFailed && !_touchFailed;
            _touchFailed = touchFailed;
            
            if ((_currentIndexPath ==  _touchingIndexPath && indexPath != _touchingIndexPath) || touchJustFailed) {
                // moving out of bounds
                unhighlightItem(indexPath, true);
                _currentIndexPath = indexPath;
                
            } else if (_currentIndexPath != _touchingIndexPath && indexPath == _touchingIndexPath) {
                 // moving back into the original touching cell
                if (!_touchFailed) {
                    highlightItem(indexPath, true, CollectionViewScrollPosition::None);
                }
                _currentIndexPath = indexPath;
            }
        }
    }
    
    void CollectionView::onTouchEnded(Touch *touch, Event *unusedEvent) {
        ScrollView::onTouchEnded(touch, unusedEvent);
        _touchId = -1;
        if (_touchingIndexPath != IndexPath::NONE) {
            // first unhighlight the touch operation
            unhighlightItem(_touchingIndexPath, true);

            const Point touchLocation = getInnerContainer()->convertToNodeSpace(touch->getLocation());
            const IndexPath indexPath = getIndexPathForItemAtPoint(touchLocation);
            if (indexPath == _touchingIndexPath && !_touchFailed) {
                userSelectedItemAtIndexPath(indexPath);
            } else if (!_allowMultipleSelection) {
                //TODO: 1233 contra
            }
            
            _touchingIndexPath = IndexPath::NONE;
            _currentIndexPath = IndexPath::NONE;
        }
    }
    
    void CollectionView::onTouchCancelled(Touch *touch, Event *unusedEvent) {
        ScrollView::onTouchCancelled(touch, unusedEvent);
        _touchId = -1;
        if (_touchingIndexPath != IndexPath::NONE) {
            unhighlightItem(_touchingIndexPath, true);
            _touchingIndexPath = IndexPath::NONE;
        }
    }
    
    
#pragma mark - Update Grid
    
    
#pragma mark - Private
    
    void CollectionView::disableTouchesRecursively(cocos2d::ui::Widget *widget) {
        widget->setTouchEnabled(false);
        for (auto &&child: widget->getChildren()) {
            Widget *childWidget = dynamic_cast<Widget *>(child);
            if (childWidget) {
                disableTouchesRecursively(childWidget);
            }
        }
    }
    
    Rect CollectionView::getVisibleBounds() const {
        const Size size = getContentSize();
        const Size contentSize = getInnerContainerSize();
        Point offset = -getInnerContainerPosition();
        offset.x = clampf(offset.x, 0, contentSize.width - size.width);
        offset.y = clampf(offset.y, 0, contentSize.height - size.height);
        return Rect(offset, size);
    }
    
    void CollectionView::invalidateLayout() {
        _layout->invalidateLayout();
        _collectionViewData->invalidate(); // invalidate layout cache
    }
    
    void CollectionView::updateVisibleCells() {       
        const auto layoutAttributesArray = _collectionViewData->getLayoutAttributesForElementsInRect(getVisibleBounds());
        
        if (layoutAttributesArray.empty()) {
            // If our layout source isn't providing any layout information, we should just
            // stop, otherwise we'll blow away all the currently existing cells.
            return;
        }
        
        std::unordered_map<CollectionViewItemKey, CollectionViewLayoutAttributes> itemKeysToAddDict;
        
        // Add new cells.
        for (auto &&layoutAttributes: layoutAttributesArray) {
            const auto itemKey = CollectionViewItemKey::make(layoutAttributes);
            itemKeysToAddDict[itemKey] = layoutAttributes;
            
            // check if cell is in visible dict; add it if not.
            auto viewIt = _allVisibleViewsDict.find(itemKey);
            if (viewIt == _allVisibleViewsDict.end()) {
                CollectionReusableView *view = nullptr;
                switch (itemKey.getType()) {
                    case CollectionViewItemType::CELL:
                        view = createPreparedCell(itemKey.getIndexPath(), layoutAttributes);
                        break;
                    case CollectionViewItemType::SUPPLEMENTARY_VIEW:
                        view = createPreparedSupplementaryView(layoutAttributes.getRepresentedElementKind(), layoutAttributes.getIndexPath(), layoutAttributes);
                        break;
                }
                
                // Supplementary views are optional
                if (view) {
                    _allVisibleViewsDict.insert(itemKey, view);
                    addControlledChild(view);
                    view->applyLayoutAttributes(layoutAttributes);
                }
                
            } else {
                // just update cell
                (*viewIt).second->applyLayoutAttributes(layoutAttributes);
            }
        }
        
        // Detect what items should be removed and queued back.
        const auto allVisibleItemKeysArray = _allVisibleViewsDict.keys();
        std::unordered_set<CollectionViewItemKey> allVisibleItemKeys(allVisibleItemKeysArray.begin(), allVisibleItemKeysArray.end());
        
        std::unordered_set<CollectionViewItemKey> itemKeysToAdd;
        for (auto &&entry: itemKeysToAddDict) {
            itemKeysToAdd.insert(entry.first);
        }
        
        std::vector<CollectionViewItemKey> keysToRemove;
        std::copy_if(allVisibleItemKeys.begin(), allVisibleItemKeys.end(), std::back_inserter(keysToRemove),
                     [&itemKeysToAdd] (const CollectionViewItemKey &needle) { return itemKeysToAdd.find(needle) == itemKeysToAdd.end(); });
               
        // Finally remove views that have not been processed and prepare them for re-use.
        for (auto &&itemKey: keysToRemove) {
            auto it = _allVisibleViewsDict.find(itemKey);
            if (it == _allVisibleViewsDict.end()) {
                continue;
            }
            CollectionReusableView *reusableView = (*it).second;
            reusableView->retain();
            reusableView->removeFromParent();
            _allVisibleViewsDict.erase(it);
            
            switch (itemKey.getType()) {
                case CollectionViewItemType::CELL:
                    reuseCell(dynamic_cast<CollectionViewCell *>(reusableView));
                    break;
                case CollectionViewItemType::SUPPLEMENTARY_VIEW:
                    reuseSupplementaryView(reusableView);
                    break;
            }
            reusableView->release();
        }
    }
    
    // fetches a cell from the dataSource and sets the layoutAttributes
    CollectionViewCell *CollectionView::createPreparedCell(const IndexPath &indexPath, const CollectionViewLayoutAttributes &attributes) {
        auto cell = _dataSource->getCellForItemAtIndexPath(this, indexPath);
        
        // Apply attributes
        cell->applyLayoutAttributes(attributes);
        
        // reset selected/highlight state
        cell->setHighlighted(_indexPathsForHighlightedItems.find(indexPath) != _indexPathsForHighlightedItems.end(), false);
        cell->setSelected(_indexPathsForSelectedItems.find(indexPath) != _indexPathsForSelectedItems.end(), false);
        
        return cell;
    }
    
    CollectionReusableView *CollectionView::createPreparedSupplementaryView(const std::string &elementKind, const IndexPath &indexPath, const CollectionViewLayoutAttributes &attributes) {
        auto view = _dataSource->getViewForSupplementaryElement(this, elementKind, indexPath);
        if (view) {
            view->applyLayoutAttributes(attributes);
            return view;
        }
        return nullptr;
    }
    
    // enqueue cell for reuse
    void CollectionView::reuseCell(CollectionViewCell *cell) {
        queueReusableView(cell, _cellReuseQueues, cell->getReuseIdentifier());
    }
    
    // enqueue supplementary view for reuse
    void CollectionView::reuseSupplementaryView(CollectionReusableView *reusableView) {
        const std::string kindAndIdentifier = reusableView->getLayoutAttributes().getRepresentedElementKind() + "/" + reusableView->getReuseIdentifier();
        queueReusableView(reusableView, _supplementaryViewReuseQueues, kindAndIdentifier);
    }
    
    void CollectionView::addControlledChild(CollectionReusableView *child) {
        addChild(child);
    }
    
    void CollectionView::highlightItem(const IndexPath &indexPath, bool animated, CollectionViewScrollPosition scrollPosition) {
        CollectionViewCell *highlightedCell = getCellForItemAtIndexPath(indexPath);
        if (highlightedCell) {
            highlightedCell->setHighlighted(true, animated);
        }
        _indexPathsForHighlightedItems.insert(indexPath);
        
        if (scrollPosition != CollectionViewScrollPosition::None) {
            scrollToItemAtIndexPath(indexPath, scrollPosition, animated);
        }
    }
    
    void CollectionView::unhighlightItem(const IndexPath &indexPath, bool animated) {
        auto highlightedItemIt = _indexPathsForHighlightedItems.find(indexPath);
        if (highlightedItemIt != _indexPathsForHighlightedItems.end()) {
            CollectionViewCell *cell = getCellForItemAtIndexPath(indexPath);
            if (cell) {
                cell->setHighlighted(false, animated);
            }
            _indexPathsForHighlightedItems.erase(highlightedItemIt);
        }
    }
    
    void CollectionView::selectItem(const IndexPath &indexPath, CollectionViewScrollPosition scrollPosition, bool animated) {
        selectItem(indexPath, scrollPosition, animated, false);
    }
    
    void CollectionView::deselectItem(const IndexPath &indexPath, bool animated) {
        deselectItem(indexPath, animated, false);
    }
    
    void CollectionView::selectItem(const IndexPath &indexPath, CollectionViewScrollPosition scrollPosition, bool animated, bool notifyDelegate) {
        if (_indexPathsForSelectedItems.find(indexPath) != _indexPathsForSelectedItems.end()) {
            deselectItem(indexPath, animated, notifyDelegate);
            
        } else {
            // either single selection, or wasn't already selected in multiple selection mode
            if (!_allowMultipleSelection && !_indexPathsForSelectedItems.empty()) {
                // now unselect the previously selected cell for single selection
                deselectItem(*_indexPathsForSelectedItems.begin(), true, true);
            }
            
            CollectionViewCell *selectedCell = getCellForItemAtIndexPath(indexPath);
            if (selectedCell) {
                selectedCell->setSelected(true, animated);
            }
            
            _indexPathsForSelectedItems.insert(indexPath);
            if (scrollPosition != CollectionViewScrollPosition::None) {
                scrollToItemAtIndexPath(indexPath, scrollPosition, animated);
            }
            if (notifyDelegate && _delegate) {
                _delegate->didSelectItemAtIndexPath(this, indexPath);
            }
        }
    }
    
    void CollectionView::deselectItem(const IndexPath &indexPath, bool animated, bool notifyDelegate) {
        auto selectedItemIt = _indexPathsForSelectedItems.find(indexPath);
        if (selectedItemIt != _indexPathsForSelectedItems.end()) {
            CollectionViewCell *selectedCell = getCellForItemAtIndexPath(indexPath);
            if (selectedCell) {
                if (selectedCell->isSelected()) {
                    selectedCell->setSelected(false, animated);
                }
            }
            _indexPathsForSelectedItems.erase(selectedItemIt);
            
            if (notifyDelegate && _delegate) {
                _delegate->didDeselectItemAtIndexPath(this, indexPath);
            }
        }
    }
    
    void CollectionView::userSelectedItemAtIndexPath(const IndexPath &indexPath) {
        if (_indexPathsForSelectedItems.find(indexPath) != _indexPathsForSelectedItems.end()) {
            deselectItem(indexPath, true, true);
        } else if (_allowSelection) {
            selectItem(indexPath, CollectionViewScrollPosition::None, true, true);
        }
    }
    
    int CollectionView::getPageNumberForContentOffset(const Vec2 offset) const {
        const bool isHorizontal = getDirection() != ScrollView::Direction::VERTICAL;
        return floor(isHorizontal
                     ? (offset.x + _pageSize.width * 0.5f) / _pageSize.width
                     : (offset.y + _pageSize.height * 0.5f) / _pageSize.height);
    }
    
}//namespace ui

NS_CC_END