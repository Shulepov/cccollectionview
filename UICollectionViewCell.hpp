//
//  UICollectionViewCell.hpp
//  Empty
//
//  Created by Mikhail Shulepov on 10.11.15.
//
//

#pragma once

#include "ui/UIWidget.h"
#include "ui/GUIExport.h"
#include "UICollectionViewLayout.hpp"


NS_CC_BEGIN

namespace ui {
    
    class CC_GUI_DLL CollectionReusableView: public Widget {
        DECLARE_CLASS_GUI_INFO
        
    public:
        CREATE_FUNC(CollectionReusableView);
        
        // Override in subclasses. Called before instance is returned to the reuse queue.
        virtual void prepareForReuse();
        
        // setup GlobalZOrder for all children for better batching
        virtual void flatten();

    protected:
        virtual Widget *createCloneInstance() override {
            return CollectionReusableView::create();
        }
        
    private:
        friend class CollectionView;
        friend class CollectionViewLayout;
        
        void setCollectionView(CollectionView *collectionView) { _collectionView = collectionView; }
        void setReuseIdentifier(const std::string &reuseIdentifier) { _reuseIdentifier = reuseIdentifier; }
        const std::string &getReuseIdentifier() const { return _reuseIdentifier; }
        const CollectionViewLayoutAttributes &getLayoutAttributes() const { return _layoutAttributes; }
        
        // Apply layout attributes on cell.
        void applyLayoutAttributes(const CollectionViewLayoutAttributes &layoutAttributes);
        void applyLayoutAttributesAnimated(const CollectionViewLayoutAttributes &layoutAttributes, float duration);
        
    private:
        CollectionViewLayoutAttributes _layoutAttributes;
        std::string _reuseIdentifier;
        CollectionView *_collectionView;
    };
    
    
    
    class CC_GUI_DLL CollectionViewCell : public CollectionReusableView {
        DECLARE_CLASS_GUI_INFO
        
    public:
        CollectionViewCell();
        CREATE_FUNC(CollectionViewCell);
        
        virtual void prepareForReuse() override;
        
        void setSelected(bool selected, bool animated) {
            if (_selected != selected) {
                _selected = selected;
                updateHighlightSelectionState(animated);
            }
        }
        bool isSelected() const { return _selected; }
        
        virtual void setHighlighted(bool highlighted, bool animated) {
            if (highlighted != isHighlighted()) {
                Widget::setHighlighted(highlighted);
                updateHighlightSelectionState(animated);
            }
        }
               
    protected:
        virtual void updateHighlightSelectionState(bool animated);
        
        virtual Widget *createCloneInstance() override {
            return CollectionViewCell::create();
        }
        
    private:
        bool _selected = false;
    };
}

NS_CC_END