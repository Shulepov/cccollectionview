//
//  UICollectionViewLayoutAttributes.hpp
//  Empty
//
//  Created by Mikhail Shulepov on 21.11.15.
//
//

#pragma once

#include "IndexPath.h"
#include "math/CCGeometry.h"

NS_CC_BEGIN

namespace ui {
    enum class CollectionViewItemType {
        CELL,
        SUPPLEMENTARY_VIEW
    };
    
    
    class CollectionViewLayoutAttributes {
    public:
        CollectionViewLayoutAttributes();
        CollectionViewLayoutAttributes(const CollectionViewLayoutAttributes &other) = default;
        
        //constructor for cell
        CollectionViewLayoutAttributes(const IndexPath &indexPath);
        
        //constructor for supplementary view
        CollectionViewLayoutAttributes(const std::string &elementKind, Index section);
        
        void setFrame(const Rect &frame);
        Rect getFrame() const;
        
        void setCenter(const Point &center);
        const Point &getCenter() const { return _center; }
        
        void setSize(const Point &size);
        const Size &getSize() const { return _size; }
        
        void setOpacity(GLubyte opacity) { _opacity = opacity; }
        GLubyte getOpacity() const { return _opacity; }
        
        void setZIndex(int zIndex) { _zIndex = zIndex; }
        int getZIndex() const { return _zIndex; }
        
        void setVisible(bool visible) { _visible = visible; }
        bool isVisible() const { return _visible; }
        
        void setSkew(const Vec2 skew) { _skew = skew; }
        const Vec2 &getSkew() const { return _skew; }
        
        void setIndexPath(const IndexPath &indexPath) { _indexPath = indexPath; }
        const IndexPath &getIndexPath() const { return _indexPath; }
        
        bool isCell() const {
            return _representedElementCategory == CollectionViewItemType::CELL;
        }
        
        std::string getRepresentedElementKind() const {
            return _representedElementKind;
        }
        
        CollectionViewItemType getRepresentedElementType() const {
            return _representedElementCategory;
        }
        
        friend bool operator ==(const CollectionViewLayoutAttributes &lhs, const CollectionViewLayoutAttributes &rhs);
        friend bool operator !=(const CollectionViewLayoutAttributes &lhs, const CollectionViewLayoutAttributes &rhs);
        
    private:
        Point _center;
        Size _size;
        Vec2 _skew;
        IndexPath _indexPath;
        int _zIndex;
        GLubyte _opacity;
        bool _visible;
        
        std::string _representedElementKind;
        CollectionViewItemType _representedElementCategory;
    };
}

NS_CC_END
