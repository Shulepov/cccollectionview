//
//  UICollectionViewCommon.h
//  Empty
//
//  Created by Mikhail Shulepov on 19.11.15.
//
//

#pragma once

#include <string>

#include "base/CCRef.h"
#include "IndexPath.h"

NS_CC_BEGIN
namespace ui {
    
    extern const std::string CollectionElementKindCell;
    extern const std::string CollectionElementKindSectionHeader;
    extern const std::string CollectionElementKindSectionFooter;
    
    class CollectionView;
    class CollectionViewCell;
    class CollectionReusableView;
    
    enum class LayoutAlignment {
        Left,
        Center,
        Right,
        Justify //default except for the last row
    };
    
    class LayoutAlignmentOptions {
    public:
        CC_SYNTHESIZE(LayoutAlignment, _alignment, Alignment);
        CC_SYNTHESIZE(LayoutAlignment, _lastRowAlignment, LastRowAlignment);
        
        LayoutAlignmentOptions()
        : _alignment(LayoutAlignment::Justify)
        , _lastRowAlignment(LayoutAlignment::Justify) {}
    };
    
    class CollectionViewDataSource {
    public:
        virtual ssize_t getNumberOfItemsInSection(CollectionView const *collectionView, ssize_t section) const = 0;
        
        // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
        virtual CollectionViewCell *getCellForItemAtIndexPath(CollectionView *collectionView, const IndexPath &indexPath) const = 0;
        
        virtual ssize_t getNumberOfSections(CollectionView const *collectionView) const { return 1; }
        
        // The view that is returned must be retrieved from a call to -dequeueReusableSupplementaryViewOfKind:withReuseIdentifier:forIndexPath:
        virtual CollectionReusableView *getViewForSupplementaryElement(CollectionView *cv, const std::string &kind, const IndexPath &indexPath) {
            return nullptr;
        }
    };
    
    class CollectionViewDelegate {
    public:
        //to disallow highlighting/selection
        virtual bool isInteractableItemAtIndexPath(CollectionView *cv, const IndexPath &indexPath) { return true; };
        
        virtual void didSelectItemAtIndexPath(CollectionView *cv, const IndexPath &indexPath) {};
        virtual void didDeselectItemAtIndexPath(CollectionView *cv, const IndexPath &indexPath) {};
    };
    
    
    //cells and supplmentary view factories
    
    class CollectionViewCellFactory {
    public:
        virtual ~CollectionViewCellFactory() {}
        virtual CollectionViewCell *createCell() = 0;
    };
    using CollectionViewCellFactoryPtr = std::shared_ptr<CollectionViewCellFactory>;
    
    class CollectionReusableViewFactory {
    public:
        virtual ~CollectionReusableViewFactory() {}
        virtual CollectionReusableView *createView() = 0;
    };
    using CollectionReusableViewFactoryPtr = std::shared_ptr<CollectionReusableViewFactory>;
}
NS_CC_END

/*

@protocol PSTCollectionViewDelegate <UIScrollViewDelegate>
@optional

// Methods for notification of selection/deselection and highlight/unhighlight events.
// The sequence of calls leading to selection from a user touch is:
//
// (when the touch begins)
// 1. -collectionView:shouldHighlightItemAtIndexPath:
// 2. -collectionView:didHighlightItemAtIndexPath:
//
// (when the touch lifts)
// 3. -collectionView:shouldSelectItemAtIndexPath: or -collectionView:shouldDeselectItemAtIndexPath:
// 4. -collectionView:didSelectItemAtIndexPath: or -collectionView:didDeselectItemAtIndexPath:
// 5. -collectionView:didUnhighlightItemAtIndexPath:
- (BOOL)collectionView:(PSTCollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionView:(PSTCollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionView:(PSTCollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath;

- (BOOL)collectionView:(PSTCollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;

- (BOOL)collectionView:(PSTCollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath; // called when the user taps on an already-selected item in multi-select mode
- (void)collectionView:(PSTCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionView:(PSTCollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionView:(PSTCollectionView *)collectionView didEndDisplayingCell:(PSTCollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionView:(PSTCollectionView *)collectionView didEndDisplayingSupplementaryView:(PSTCollectionReusableView *)view forElementOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath;

// These methods provide support for copy/paste actions on cells.
// All three should be implemented if any are.
- (BOOL)collectionView:(PSTCollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath;

- (BOOL)collectionView:(PSTCollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender;

- (void)collectionView:(PSTCollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender;

@end*/