//
//  UICollectionViewData.hpp
//  Empty
//
//  Created by Mikhail Shulepov on 11.11.15.
//
//

#pragma once

#include "base/CCRef.h"

#include "IndexPath.h"
#include "UICollectionViewLayoutAttributes.hpp"

NS_CC_BEGIN

namespace ui {
    class CollectionView;
    class CollectionViewLayout;
    
    class CollectionViewData: public Ref {
    public:
        static CollectionViewData *create(CollectionView *collectionView, CollectionViewLayout *layout) {
            auto ret = new CollectionViewData(collectionView, layout);
            ret->autorelease();
            return ret;
        }
        
        CollectionViewData(CollectionView *collectionView, CollectionViewLayout *layout);
        
        // Ensure data is valid. may fetches items from dataSource and layout.
        void validateLayoutInRect(const Rect &rect);
        //Rect rectForItemAtIndexPath(const IndexPath &indexPath) const;

        Index getGlobalIndexForItemAtIndexPath(const IndexPath &indexPath);
        IndexPath getIndexPathForItemAtGlobalIndex(Index index);
        
        // Fetch layout attributes
        std::vector<CollectionViewLayoutAttributes> getLayoutAttributesForElementsInRect(const Rect &rect);
        
        // Make data to re-evaluate dataSources.
        void invalidate();
        
        // Access cached item data
        //- (NSInteger)numberOfItemsBeforeSection:(NSInteger)section;
        
        //- (NSInteger)numberOfItemsInSection:(NSInteger)section;
        
        //- (NSInteger)numberOfItems;
        
        //- (NSInteger)numberOfSections;
        
        // Total size of the content.
        //- (CGRect)collectionViewContentRect;
        
        //@property (readonly) BOOL layoutIsPrepared;
        
    private:
        void validateItemsCount();
        void prepareToLoadData();
        ssize_t getNumberOfItemsBeforeSection(Index section);
        void updateItemsCount();
        
    private:
        CollectionView *_collectionView;
        CollectionViewLayout *_layout;
        
        struct {
            unsigned int contentSizeIsValid:1;
            unsigned int itemCountsAreValid:1;
            unsigned int layoutIsPrepared:1;
        } _collectionViewDataFlags;
        
        cocos2d::Rect _validLayoutRect;
        std::vector<CollectionViewLayoutAttributes> _cachedLayoutAttributes;
        cocos2d::Size _contentSize;
        
        ssize_t _numItems;
        ssize_t _numSections;
        std::vector<ssize_t> _sectionItemCounts;
    };
}

NS_CC_END