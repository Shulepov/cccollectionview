//
//  UIGridLayoutInfo.cpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 17.12.15.
//
//

#include "UIGridLayoutInfo.hpp"
#include "UIGridLayoutSection.hpp"
#include "UIGridLayoutItem.hpp"
#include "UIGridLayoutRow.hpp"

NS_CC_BEGIN
namespace ui {
    GridLayoutInfo *GridLayoutInfo::stanpshot() const {
        GridLayoutInfo *copy = new GridLayoutInfo;
        copy->_sections = _sections;
        copy->setUsesFloatingHeaderFooter(_floatingHeaderFooter);
        copy->setAlignmentOptions(_alignmentOptions);
        copy->setDimension(_dimension);
        copy->setHorizontal(_horizontal);
        copy->setLeftToRight(_leftToRight);
        copy->setContentSize(_contentSize);
        copy->autorelease();
        return copy;
    }
    
    Rect GridLayoutInfo::getFrameForItemAtIndexPath(const IndexPath &indexPath) {
        auto section = _sections.at(indexPath.getSection());
        Rect itemFrame;
        if (section->getFixedItemSize()) {
            itemFrame = Rect(Point::ZERO, section->getItemSize());
        } else {
            itemFrame = section->getItem(indexPath.getItem())->getFrame();
        }
        return itemFrame;
    }
    
    GridLayoutSection *GridLayoutInfo::addSection() {
        GridLayoutSection *section = new GridLayoutSection;
        section->setAlignmentOptions(_alignmentOptions);
        section->setLayoutInfo(this);
        _sections.pushBack(section);
        invalidate();
        section->autorelease();
        return section;
    }
    
    void GridLayoutInfo::invalidate() {
        _isValid = false;
    }
}
NS_CC_END