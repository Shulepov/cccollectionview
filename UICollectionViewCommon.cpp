//
//  UICollectionViewCommon.cpp
//  Empty
//
//  Created by Mikhail Shulepov on 21.11.15.
//
//

#include "UICollectionViewCommon.h"


NS_CC_BEGIN
namespace ui {
    const std::string CollectionElementKindCell = "UICollectionElementKindCell";
    const std::string CollectionElementKindSectionHeader = "UICollectionElementKindSectionHeader";
    const std::string CollectionElementKindSectionFooter = "UICollectionElementKindSectionFooter";
}
NS_CC_END