//
//  UIGridLayoutInfo.hpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 17.12.15.
//
//

#pragma once

#include "base/CCRef.h"
#include "base/CCVector.h"
#include "math/CCGeometry.h"
#include "IndexPath.h"
#include "UICollectionViewCommon.h"

NS_CC_BEGIN
namespace ui {
    class GridLayoutSection;
    
    /*
     Every CollectionViewLayout has a GridLayoutInfo attached.
     Is used extensively in CollectionViewFlowLayout.
     */
    class GridLayoutInfo: public cocos2d::Ref {
    public:
        CC_SYNTHESIZE(bool, _floatingHeaderFooter, UsesFloatingHeaderFooter);
        
        CC_SYNTHESIZE(float, _dimension, Dimension);
        
        CC_SYNTHESIZE(bool, _horizontal, Horizontal);
        CC_SYNTHESIZE(bool, _leftToRight, LeftToRight);
        CC_SYNTHESIZE(Size, _contentSize, ContentSize);
        CC_SYNTHESIZE(LayoutAlignmentOptions, _alignmentOptions, AlignmentOptions);
        
        const cocos2d::Vector<GridLayoutSection *> &getSections() const { return _sections; }
        
        Rect getFrameForItemAtIndexPath(const IndexPath &indexPath);
        
        // Add new section. Invalidates layout.
        GridLayoutSection *addSection();
        
        // forces the layout to recompute on next access
        void invalidate();
        
        // Make a copy of the current state.
        GridLayoutInfo *stanpshot() const;
        
    private:
        Rect _visibleBounds;
        Size _layoutSize;
        bool _isValid = false;
        
        cocos2d::Vector<GridLayoutSection *> _sections;
    };
}
NS_CC_END
