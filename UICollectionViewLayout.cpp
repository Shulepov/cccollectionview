//
//  UICollectionViewLayout.cpp
//  Empty
//
//  Created by Mikhail Shulepov on 10.11.15.
//
//

#include "UICollectionViewLayout.hpp"
#include "UICollectionView.hpp"
#include "UICollectionViewData.hpp"

NS_CC_BEGIN

namespace ui {
    void CollectionViewLayout::invalidateLayout() {
        //TODO: uncomment or not?
        //[[_collectionView collectionViewData] invalidate];
        //[_collectionView setNeedsLayout];
    }
    
    bool CollectionViewLayout::shouldInvalidateLayoutForBoundsChange(const Rect &newBounds) const {
        return true;
    }
    
    Vec2 CollectionViewLayout::getTargetContentOffsetForProposedContentOffset(const Vec2 &proposedContentOffset, const Vec2 &scrollingVelocity) const {
        return proposedContentOffset;
    }
    
    void CollectionViewLayout::prepareForCollectionViewUpdates(const std::vector<CollectionViewUpdateItem> &updateItems) {
        const auto update = _collectionView->getCurrentUpdate();

        //initial animations
        const auto visibleViews = _collectionView->getVisibleViews();
        for (auto view: visibleViews) {
            auto attr = view->getLayoutAttributes();
            _initialAnimationLayoutAttributesDict.emplace(CollectionViewItemKey::make(attr), attr);
        }
        
        //final animations
        auto data = _collectionView->getCollectionViewData();
        const Rect bounds = Rect(
            -_collectionView->getInnerContainerPosition(),
            _collectionView->getContentSize()
        );
        
        auto attributesInBounds = data->getLayoutAttributesForElementsInRect(bounds);
        for (auto &&attr: attributesInBounds) {
            if (attr.isCell()) {
                Index idx = data->getGlobalIndexForItemAtIndexPath(attr.getIndexPath());
                idx = update.newToOldIndexMap[idx];
                if (idx != IndexNotFound) {
                    auto finalAttrs(attr);
                    finalAttrs.setIndexPath(update.oldModel->getIndexPathForItemAtGlobalIndex(idx));
                    finalAttrs.setOpacity(0);
                    _finalAnimationLayoutAttributesDict[CollectionViewItemKey::make(finalAttrs)] = finalAttrs;
                }
            }
        }
        
        //
        for (auto &&updateItem: updateItems) {
            CollectionUpdateAction action = updateItem.getUpdateAction();
            if (updateItem.isSectionOperation()) {
                switch (action) {
                    case CollectionUpdateAction::RELOAD:
                    case CollectionUpdateAction::MOVE:
                        _deletedSectionsSet.insert(updateItem.getIndexPathBeforeUpdate().getSection());
                        _insertedSectionsSet.insert(updateItem.getIndexPathAfterUpdate().getSection());
                        break;
                    case CollectionUpdateAction::INSERT:
                        _insertedSectionsSet.insert(updateItem.getIndexPathAfterUpdate().getSection());
                        break;
                    case CollectionUpdateAction::DELETE:
                        _insertedSectionsSet.insert(updateItem.getIndexPathBeforeUpdate().getSection());
                        break;
                    case CollectionUpdateAction::NONE:
                        break;
                }
                
                
            } else {
                switch (action) {
                    case CollectionUpdateAction::DELETE: {
                        const CollectionViewItemKey key = CollectionViewItemKey::make(updateItem.getIndexPathBeforeUpdate());
                        auto it = _finalAnimationLayoutAttributesDict.find(key);
                        if (it != _finalAnimationLayoutAttributesDict.end()) {
                            (*it).second.setOpacity(0);
                        }
                        break;
                    }
                    case CollectionUpdateAction::RELOAD:
                    case CollectionUpdateAction::INSERT: {
                        const CollectionViewItemKey key = CollectionViewItemKey::make(updateItem.getIndexPathAfterUpdate());
                        auto it = _initialAnimationLayoutAttributesDict.find(key);
                        if (it != _initialAnimationLayoutAttributesDict.end()) {
                            (*it).second.setOpacity(0);
                        }
                        break;
                    }
                    case CollectionUpdateAction::MOVE:
#warning no implementation for move
                        break;
                    case CollectionUpdateAction::NONE:
                        break;
                }
            }
        }
    }
    
    std::shared_ptr<CollectionViewLayoutAttributes> CollectionViewLayout::getInitialLayoutAttributesForAppearingItem(const IndexPath &itemIndexPath) {
        const CollectionViewItemKey key = CollectionViewItemKey::make(itemIndexPath);
        auto it = _initialAnimationLayoutAttributesDict.find(key);
        if (it != _initialAnimationLayoutAttributesDict.end()) {
            auto attrs = std::make_shared<CollectionViewLayoutAttributes>((*it).second);
            if (_insertedSectionsSet.find(itemIndexPath.getSection()) != _insertedSectionsSet.end()) {
                attrs->setOpacity(0);
            }
            return attrs;
        }
        assert(false);
    }
    
    std::shared_ptr<CollectionViewLayoutAttributes> CollectionViewLayout::getFinalLayoutAttributesForDisappearingItem(const IndexPath &itemIndexPath) {
        const CollectionViewItemKey key = CollectionViewItemKey::make(itemIndexPath);
        auto it = _finalAnimationLayoutAttributesDict.find(key);
        if (it != _finalAnimationLayoutAttributesDict.end()) {
            auto attrs =std::make_shared<CollectionViewLayoutAttributes>((*it).second);
            if (_deletedSectionsSet.find(itemIndexPath.getSection()) != _deletedSectionsSet.end()) {
                attrs->setOpacity(0);
            }
            return attrs;
        }
        assert(false);
    }
    
    std::shared_ptr<CollectionViewLayoutAttributes> CollectionViewLayout::getInitialLayoutAttributesForInsertedSupplementaryElement(const std::string &elementKind, const IndexPath &elementIndexPath) {
        const CollectionViewItemKey key = CollectionViewItemKey::makeForSupplementaryView(elementIndexPath, elementKind);
        auto it = _initialAnimationLayoutAttributesDict.find(key);
        if (it != _initialAnimationLayoutAttributesDict.end()) {
            auto attrs = std::make_shared<CollectionViewLayoutAttributes>((*it).second);
            if (_insertedSectionsSet.find(elementIndexPath.getSection()) != _insertedSectionsSet.end()) {
                attrs->setOpacity(0);
            }
            return attrs;
        }
        return nullptr;
    }
    
    std::shared_ptr<CollectionViewLayoutAttributes> CollectionViewLayout::getFinalLayoutAttributesForDeletedSupplementaryElement(const std::string &elementKind, const IndexPath &elementIndexPath) {
        return nullptr;
    }
    
    void CollectionViewLayout::finalizeCollectionViewUpdates() {
        _initialAnimationLayoutAttributesDict.clear();
        _finalAnimationLayoutAttributesDict.clear();
        _deletedSectionsSet.clear();
        _insertedSectionsSet.clear();
    }
}

NS_CC_END