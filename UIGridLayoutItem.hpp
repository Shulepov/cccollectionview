//
//  GridLayoutItem.hpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 17.12.15.
//
//

#pragma once

#include "base/CCRef.h"
#include "math/CCGeometry.h"

NS_CC_BEGIN
namespace ui {
    class GridLayoutSection;
    class GridLayoutRow;
    
    // Represents a single grid item; only created for non-uniform-sized grids.
    class GridLayoutItem: public cocos2d::Ref {
    public:
        //unretained
        CC_SYNTHESIZE(GridLayoutSection *, _section, Section);
        CC_SYNTHESIZE(GridLayoutRow *, _row, Row);
        CC_SYNTHESIZE_PASS_BY_REF(Rect, _frame, Frame);      
    };
}
NS_CC_END