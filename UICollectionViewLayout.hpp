//
//  UICollectionViewLayout.hpp
//  Empty
//
//  Created by Mikhail Shulepov on 10.11.15.
//
//

#pragma once

#include "ui/GUIExport.h"
#include "IndexPath.h"
#include "UICollectionViewUpdateItem.hpp"
#include "UICollectionViewItemKey.hpp"

NS_CC_BEGIN

namespace ui {
    class CollectionView;
    
    class CollectionViewLayout: public Ref {
    public:
        
        CollectionView *getCollectionView() const { return _collectionView; }
        
        // Call -invalidateLayout to indicate that the collection view needs to requery the layout information.
        // Subclasses must always call super if they override.
        virtual void invalidateLayout();
        
        // The collection view calls -prepareLayout once at its first layout as the first message to the layout instance.
        // The collection view calls -prepareLayout again after layout is invalidated and before requerying the layout information.
        // Subclasses should always call super if they override.
        virtual void prepareLayout() {}
        
        // PSTCollectionView calls these four methods to determine the layout information.
        // Implement -layoutAttributesForElementsInRect: to return layout attributes for supplementary or decoration views, or to perform layout in an as-needed-on-screen fashion.
        // Additionally, all layout subclasses should implement -layoutAttributesForItemAtIndexPath: to return layout attributes instances on demand for specific index paths.
        // If the layout supports any supplementary or decoration view types, it should also implement the respective atIndexPath: methods for those types.
        virtual std::vector<CollectionViewLayoutAttributes> getLayoutAttributesForElementsInRect(const Rect &rect) = 0;
        
        
        virtual CollectionViewLayoutAttributes getLayoutAttributesForItem(const IndexPath &indexPath) = 0;
        virtual std::shared_ptr<CollectionViewLayoutAttributes> getLayoutAttributesForSupplementaryView(const std::string &kind, const IndexPath &indexPath) = 0;
        
        virtual bool shouldInvalidateLayoutForBoundsChange(const Rect &newBounds) const; // return YES to cause the collection view to requery the layout for geometry information
        
        // return a point at which to rest after scrolling - for layouts that want snap-to-point scrolling behavior
        virtual Vec2 getTargetContentOffsetForProposedContentOffset(const Vec2 &proposedContentOffset, const Vec2 &scrollingVelocity) const;
        
        // the collection view calls this to update its content size any time it queries new layout information - at least one of the width and height fields must match the respective field of the collection view's bounds
        virtual Size getCollectionViewContentSize() = 0;
        
        //
        // Updates hooks
        //
        
        virtual void prepareForCollectionViewUpdates(const std::vector<CollectionViewUpdateItem> &updateItems);
        virtual void finalizeCollectionViewUpdates();
        
        // Collection view calls these methods to determine the starting layout for animating in newly inserted views, or the ending layout for animating out deleted views
        std::shared_ptr<CollectionViewLayoutAttributes> getInitialLayoutAttributesForAppearingItem(const IndexPath &itemIndexPath);
        
        std::shared_ptr<CollectionViewLayoutAttributes> getFinalLayoutAttributesForDisappearingItem(const IndexPath &itemIndexPath);
        
        std::shared_ptr<CollectionViewLayoutAttributes> getInitialLayoutAttributesForInsertedSupplementaryElement(const std::string &elementKind, const IndexPath &elementIndexPath);
        
        std::shared_ptr<CollectionViewLayoutAttributes> getFinalLayoutAttributesForDeletedSupplementaryElement(const std::string &elementKind, const IndexPath &elementIndexPath);
                
    private:
        friend class CollectionView;
        
        CollectionView *_collectionView;
        std::unordered_map<CollectionViewItemKey, CollectionViewLayoutAttributes> _initialAnimationLayoutAttributesDict;
        std::unordered_map<CollectionViewItemKey, CollectionViewLayoutAttributes> _finalAnimationLayoutAttributesDict;
        
        std::set<Index> _deletedSectionsSet;
        std::set<Index> _insertedSectionsSet;
    };
}

NS_CC_END