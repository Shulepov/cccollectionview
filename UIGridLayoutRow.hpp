//
//  UIGridLayoutRow.hpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 17.12.15.
//
//

#pragma once

#include "base/CCRef.h"
#include "base/CCVector.h"
#include "math/CCGeometry.h"
#include "IndexPath.h"

NS_CC_BEGIN
namespace ui {
    class GridLayoutSection;
    class GridLayoutItem;
    
    class GridLayoutRow: public cocos2d::Ref {
    public:
        CC_SYNTHESIZE(GridLayoutSection *, _section, Section);
        CC_SYNTHESIZE(Size, _rowSize, RowSize);
        CC_SYNTHESIZE(Rect, _rowFrame, RowFrame);
        CC_SYNTHESIZE(Index, _index, Index);
        CC_SYNTHESIZE(bool, _complete, Complete);
        CC_SYNTHESIZE(bool, _fixedItemSize, FixedItemSize);
        
        const Vector<GridLayoutItem *> &getItems() const { return _items; }
        
        // Add new item to items array.
        void addItem(GridLayoutItem *item);
        
        // Layout current row (if invalid)
        void layoutRow();
        
        //Helper to save code in CollectionViewFlowLayout.
        //Returns the item rects when fixedItemSize is enabled.
        std::vector<Rect> getItemRects();
        
        //  Set current row frame invalid.
        void invalidate();
        
        // Copy a snapshot of the current row data
        GridLayoutRow *snapshot() const;
        
        ssize_t getItemCount() const {
            return _fixedItemSize ? _itemCount : _items.size();
        }
        
        void setItemCount(ssize_t itemCount) {
            _itemCount = itemCount;
        }
        
    private:
        std::vector<Rect> layoutRowAndGenerateRectArray(bool generateRectArray);
        
    private:
        cocos2d::Vector<GridLayoutItem *> _items;
        bool _isValid = false;
        int _verticalAlignment = 0;
        int _horizontalAlignment = 0;
        ssize_t _itemCount = 0;
    };
}
NS_CC_END