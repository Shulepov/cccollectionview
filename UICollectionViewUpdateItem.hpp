//
//  UICollectionViewUpdateItem.hpp
//  Empty
//
//  Created by Mikhail Shulepov on 11.11.15.
//
//

#pragma once

#include "ui/UIWidget.h"
#include "ui/GUIExport.h"
#include "IndexPath.h"

NS_CC_BEGIN

namespace ui {
    enum class CollectionUpdateAction {
        INSERT,
        DELETE,
        RELOAD,
        MOVE,
        NONE
    };
    
    class CollectionViewUpdateItem {
    public:
        
        IndexPath getIndexPathBeforeUpdate() const { return _indexPathBeforeUpdate; }
        IndexPath getIndexPathAfterUpdate() const { return _indexPathAfterUpdate; }
        CollectionUpdateAction getUpdateAction() const { return _updateAction; }
        
        CollectionViewUpdateItem(const IndexPath &initialIndexPath, const IndexPath &finalIndexPath, CollectionUpdateAction action);
        
        static CollectionViewUpdateItem create(CollectionUpdateAction action, const IndexPath &indexPath);
        static CollectionViewUpdateItem createMoveUpdate(const IndexPath &oldIndexPath, const IndexPath &newIndexPath);
        
        int compareIndexPaths(const CollectionViewUpdateItem &otherItem) const;
        int inverseCompareIndexPaths(const CollectionViewUpdateItem &otherItem) const;
        
        //should be private
        bool isSectionOperation() const;
        
    private:
        IndexPath _indexPathBeforeUpdate;
        IndexPath _indexPathAfterUpdate;
        CollectionUpdateAction _updateAction;
    };
}

NS_CC_END