//
//  UIGridLayoutRow.cpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 17.12.15.
//
//

#include "UIGridLayoutRow.hpp"
#include "UIGridLayoutItem.hpp"
#include "UIGridLayoutSection.hpp"
#include "UIGridLayoutInfo.hpp"

NS_CC_BEGIN
namespace ui {
    
#pragma mark - Public
    
    void GridLayoutRow::invalidate() {
        _isValid = false;
        _rowSize = Size::ZERO;
        _rowFrame = Rect::ZERO;
    }
    
    std::vector<Rect> GridLayoutRow::getItemRects() {
        return layoutRowAndGenerateRectArray(true);
    }
    
    void  GridLayoutRow::layoutRow() {
        layoutRowAndGenerateRectArray(false);
    }
    
    void GridLayoutRow::addItem(GridLayoutItem *item) {
        _items.pushBack(item);
        item->setRow(this);
        invalidate();
    }
    
    GridLayoutRow *GridLayoutRow::snapshot() const {
        GridLayoutRow *copy = new GridLayoutRow;
        copy->setSection(_section);
        copy->_items = _items;
        copy->setRowSize(_rowSize);
        copy->setRowFrame(_rowFrame);
        copy->setIndex(_index);
        copy->setComplete(_complete);
        copy->setFixedItemSize(_fixedItemSize);
        copy->setItemCount(_itemCount);
        return copy;
    }
    
#pragma mark - Private
    
    std::vector<Rect> GridLayoutRow::layoutRowAndGenerateRectArray(bool generateRectArray) {
        std::vector<Rect> rects;
        if (!_isValid || generateRectArray) {
            // properties for aligning
            const bool isHorizontal = _section->getLayoutInfo()->getHorizontal();
            const bool isLastRow = _section->getIndexOfIncompleteRow() == _index;
            const auto alignmentOptions = _section->getAlignmentOptions();
            const LayoutAlignment horizontalAlignment = isLastRow
                ? alignmentOptions.getLastRowAlignment()
                : alignmentOptions.getAlignment();
            
            // calculate space that's left over if we would align it from left to right.
            float leftOverSpace = _section->getLayoutInfo()->getDimension();
            const Margin sectionMargins = _section->getSectionMargins();
            if (isHorizontal) {
                leftOverSpace -= sectionMargins.top + sectionMargins.bottom;
            } else {
                leftOverSpace -= sectionMargins.left + sectionMargins.right;
            }
            
            // calculate the space that we have left after counting all items.
            // CollectionView is smart and lays out items like they would have been placed on a full row
            // So we need to calculate the "usedItemCount" with using the last item as a reference size.
            // This allows us to correctly justify-place the items in the grid.
            ssize_t usedItemCount = 0;
            ssize_t itemIndex = 0;
            float spacing = isHorizontal
                ? _section->getVerticalInterstice()
                : _section->getHorizontalInterstice();
            
            const ssize_t itemCount = getItemCount();
            
            // the last row should justify as if it is filled with more (invisible) items so that the whole
            // CollectionView feels more like a grid than a random line of blocks
            while (itemIndex < itemCount || isLastRow) {
                float nextItemSize;
                // first we need to find the size (width/height) of the next item to fit
                if (!_fixedItemSize) {
                    GridLayoutItem *item = _items.at(std::min(itemIndex, itemCount - 1));
                    const Rect &itemFrame = item->getFrame();
                    nextItemSize = isHorizontal ? itemFrame.size.height : itemFrame.size.width;
                } else {
                    const Size &itemSize = _section->getItemSize();
                    nextItemSize = isHorizontal ? itemSize.height : itemSize.width;
                }
                
                // the first item does not add a separator spacing,
                // every one afterwards in the same row will need this spacing constant
                if (itemIndex > 0) {
                    nextItemSize += spacing;
                }
                
                // check to see if we can at least fit an item (+separator if necessary)
                if (leftOverSpace < nextItemSize) {
                    break;
                }
                
                // we need to maintain the leftover space after the maximum amount of items have
                // occupied, so we know how to adjust equal spacing among all the items in a row
                leftOverSpace -= nextItemSize;
                
                itemIndex++;
                usedItemCount = itemIndex;
            } //end while (itemIndex < itemCount || isLastRow)
            
            // push everything to the right if right-aligning and divide in half for centered
            // currently there is no public API supporting this behavior
            Point itemOffset = Point::ZERO;
            if (horizontalAlignment == LayoutAlignment::Right) {
                itemOffset.x += leftOverSpace;
            } else if (horizontalAlignment == LayoutAlignment::Center ||
                       (horizontalAlignment == LayoutAlignment::Justify && usedItemCount == 1)) {
                // Special case one item row to split leftover space in half
                itemOffset.x += leftOverSpace / 2;
            }
            
            // calculate the justified spacing among all items in a row if we are using
            // the default PSTFlowLayoutHorizontalAlignmentJustify layout
            const float interSpacing = usedItemCount <= 1 ? 0 : (leftOverSpace / (usedItemCount - 1));
                       
            // calculate row frame as union of all items
            Rect frame = Rect::ZERO;
            Rect itemFrame = Rect(Point::ZERO, _section->getItemSize());
            for (itemIndex = 0; itemIndex < itemCount; ++itemIndex) {
                GridLayoutItem *item = nullptr;
                if (!_fixedItemSize) {
                    item = _items.at(itemIndex);
                    itemFrame = item->getFrame();
                }
                // depending on horizontal/vertical for an item size (height/width),
                // we add the minimum separator then an equally distributed spacing
                // (since our default mode is justify) calculated from the total leftover
                // space divided by the number of intervals
                if (isHorizontal) {
                    itemFrame.origin.y = itemOffset.y;
                    itemOffset.y += itemFrame.size.height + _section->getVerticalInterstice();
                    if (horizontalAlignment == LayoutAlignment::Justify) {
                        itemOffset.y += interSpacing;
                    }
                } else {
                    itemFrame.origin.x = itemOffset.x;
                    itemOffset.x += itemFrame.size.width + _section->getHorizontalInterstice();
                    if (horizontalAlignment == LayoutAlignment::Justify) {
                        itemOffset.x += interSpacing;
                    }
                }
                if (item) {
                    item->setFrame(itemFrame);
                }
                rects.push_back(itemFrame);
                frame = frame.unionWithRect(itemFrame);
            }//end for items
            _rowSize = frame.size;
            //        _rowFrame = frame; // set externally
            _isValid = true;
        }
        
        return rects;
    }
}
NS_CC_END