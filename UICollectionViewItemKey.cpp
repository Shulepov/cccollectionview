//
//  UICollectionViewItemKey.cpp
//  Empty
//
//  Created by Mikhail Shulepov on 12.11.15.
//
//

#include <functional>

#include "UICollectionViewItemKey.hpp"
#include "UICollectionViewCommon.h"

bool cocos2d::ui::CollectionViewItemKey::operator ==(const cocos2d::ui::CollectionViewItemKey &b) const {
    return _hash == b._hash;
}

bool cocos2d::ui::CollectionViewItemKey::operator <(const cocos2d::ui::CollectionViewItemKey &b) const {
    return _hash < b._hash;
}

NS_CC_BEGIN

namespace ui {
    
    CollectionViewItemKey::CollectionViewItemKey(const CollectionViewItemKey &other)
    : _hash(other._hash)
    , _type(other._type)
    , _indexPath(other._indexPath) {
        
    }
    
    CollectionViewItemKey CollectionViewItemKey::make(const CollectionViewLayoutAttributes &layoutAttributes) {
        return make(layoutAttributes.getIndexPath(), layoutAttributes.getRepresentedElementType(), layoutAttributes.getRepresentedElementKind());
    }
    
    CollectionViewItemKey CollectionViewItemKey::make(const IndexPath &indexPath) {
        return make(indexPath, CollectionViewItemType::CELL, CollectionElementKindCell);
    }
    
    CollectionViewItemKey CollectionViewItemKey::makeForSupplementaryView(const IndexPath &indexPath, const std::string &elementKind) {
        return make(indexPath, CollectionViewItemType::SUPPLEMENTARY_VIEW, elementKind);
    }
    
    CollectionViewItemKey CollectionViewItemKey::make(const IndexPath &indexPath, CollectionViewItemType type, const std::string &identifier) {
        std::string typeName;
        switch (type) {
            case CollectionViewItemType::CELL:
                typeName = "Cell";
                break;
            case CollectionViewItemType::SUPPLEMENTARY_VIEW:
                typeName = "Supplementary";
                break;
        }
        std::hash<std::string> hasher;
        CollectionViewItemKey ret;
        ret._hash = ((indexPath.getSection() * 31 + indexPath.getItem() * 7) + hasher(typeName)) * 17 + hasher(identifier);
        ret._type = type;
        ret._indexPath = indexPath;
        
        static std::map<ssize_t, CollectionViewItemKey> all;
        auto it = all.find(ret._hash);
        if (it != all.end()) {
            CollectionViewItemKey old = (*it).second;
        } else {
            all.emplace(ret._hash, ret);
        }
        
        return ret;
    }
}

NS_CC_END