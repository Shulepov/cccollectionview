//
//  UIGridLayoutSection.hpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 17.12.15.
//
//

#pragma once

#include "base/CCRef.h"
#include "base/CCVector.h"
#include "math/CCGeometry.h"
#include "IndexPath.h"
#include "Margin.h"
#include "UICollectionViewCommon.h"

NS_CC_BEGIN
namespace ui {
    class GridLayoutInfo;
    class GridLayoutRow;
    class GridLayoutItem;
    
    class GridLayoutSection: public cocos2d::Ref {
    public:
        // fast path for equal-size items
        CC_SYNTHESIZE(bool, _fixedItemSize, FixedItemSize);
        CC_SYNTHESIZE_PASS_BY_REF(Size, _itemSize, ItemSize);
        
        CC_SYNTHESIZE(float, _verticalInterstice, VerticalInterstice);
        CC_SYNTHESIZE(float, _horizontalInterstice, HorizontalInterstice);
        CC_SYNTHESIZE_PASS_BY_REF(Margin, _sectionMargins, SectionMargins);
        
        CC_SYNTHESIZE_PASS_BY_REF(Rect, _frame, Frame);
        CC_SYNTHESIZE_PASS_BY_REF(Rect, _headerFrame, HeaderFrame);
        CC_SYNTHESIZE_PASS_BY_REF(Rect, _footerFrame, FooterFrame);
        CC_SYNTHESIZE(float, _headerDimension, HeaderDimension);
        CC_SYNTHESIZE(float, _footerDimension, FooterDimension);
        
        CC_SYNTHESIZE(GridLayoutInfo *, _layoutInfo, LayoutInfo);
        CC_SYNTHESIZE(LayoutAlignmentOptions, _alignmentOptions, AlignmentOptions);
        
        void setItemsCount(ssize_t count) {
            _itemsCount = count;
        }
        
        ssize_t getItemsCount() const {
            return _fixedItemSize ? _itemsCount : _items.size();
        }
        
        const Vector<GridLayoutRow *> &getRows() const { return _rows; }
        ssize_t getRowsCount() const { return _rows.size(); }
        
        GridLayoutItem *getItem(Index index) const {
            return _items.at(index);
        }
        Index getIndexOfItem(GridLayoutItem *item) const {
            auto it = std::find(_items.begin(), _items.end(), item);
            return std::distance(_items.begin(), it);
        }
        
        float getOtherMargin() const;
        float getBeginMargin() const;
        float getEndMargin() const;
        float getActualGap() const;
        float getLastRowBeginMargin() const;
        float getLastRowEndMargin() const;
        float getLastRowActualGap() const;
        bool isLastRowIncomplete() const;
        ssize_t getItemsByRowCount() const { return _itemsByRowCount; }
        Index getIndexOfIncompleteRow() const { return _indexOfIncompleteRow; }
        
        // Faster variant of invalidate/compute
        void recomputeFromIndex(Index index);
        
        // Invalidate layout. Destroys rows.
        void invalidate();
        
        // Compute layout. Creates rows.
        void computeLayout();
        
        GridLayoutItem *addItem();
        GridLayoutRow *addRow();
        
        GridLayoutSection *snapshot() const;
        
    private:
        Vector<GridLayoutItem *> _items;
        Vector<GridLayoutRow *> _rows;
        
        bool _isValid = false;
        ssize_t _itemsByRowCount = 0;
        Index _indexOfIncompleteRow = 0;
        ssize_t _itemsCount;
    };
}
NS_CC_END