//
//  UICollectionView.hpp
//  Empty
//
//  Created by Mikhail Shulepov on 10.11.15.
//
//

#pragma once

#include <map>
#include <vector>
#include <set>
#include <queue>
#include <unordered_map>

#include "ui/UIScrollView.h"
#include "ui/GUIExport.h"
#include "IndexPath.h"
#include "UICollectionViewCell.hpp"
#include "UICollectionViewUpdateItem.hpp"
#include "UICollectionView_private.h"
#include "UICollectionViewCommon.h"

NS_CC_BEGIN

namespace ui {
    class CollectionViewLayout;
    class CollectionViewData;
    class CollectionReusableView;
    class CollectionViewCell;
    
    enum class CollectionViewScrollPosition {
        None,
        
        Top,
        CenteredVertically,
        Bottom,
        
        Left,
        CenteredHorizontally,
        Right
    };
    
    class CC_GUI_DLL CollectionView : public ScrollView {
        DECLARE_CLASS_GUI_INFO

    public:
        static CollectionView *create(RefPtr<CollectionViewLayout> layout) {
            auto ret = new CollectionView;
            ret->init(layout);
            ret->autorelease();
            return ret;
        }
        
        virtual bool init(RefPtr<CollectionViewLayout> layout);
        
        CC_SYNTHESIZE(CollectionViewDelegate *, _delegate, Delegate);
        CC_SYNTHESIZE(CollectionViewDataSource *, _dataSource, DataSource);
        
        Vector<CollectionReusableView *> getVisibleViews() const;
        
        // Paging
        CC_SYNTHESIZE_READONLY_PASS_BY_REF(Size, _pageSize, PageSize);
        void setPageSize(const Size &size);
        
        // View reusing
        void registerReusableCell(CollectionViewCellFactoryPtr cellFactory, const std::string &reuseIdentifier);
        void registerSupplementaryView(CollectionReusableViewFactoryPtr viewFactory, const std::string &kind, const std::string &reuseIdentifier);
        CollectionViewCell *getDequeueReusableCell(const std::string &reuseIdentifier, const IndexPath &indexPath);
        CollectionReusableView *getDequeueReusableSupplementaryView(const std::string &reuseIdentifier, const std::string &kind, const IndexPath &indexPath);
        
        // sSelection
        CC_SYNTHESIZE(bool, _allowSelection, AllowSelection);
        CC_SYNTHESIZE(bool, _allowMultipleSelection, AllowMultipleSelection);
        std::vector<IndexPath> getIndexPathsForSelectedItems() const;
        //- (void)selectItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated scrollPosition:(PSTCollectionViewScrollPosition)scrollPosition;
        //- (void)deselectItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated;
        
        
        // Updating data
        void reloadData();
        //- (void)setCollectionViewLayout:(PSTCollectionViewLayout *)layout animated:(BOOL)animated; // transition from one layout to another
        
        ssize_t getNumberOfSections() const;
        ssize_t getNumberOfItemsInSection(Index section) const;
        
        // Information about the current state of the collection view.
        //- (PSTCollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath;
        //- (PSTCollectionViewLayoutAttributes *)layoutAttributesForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath;
        IndexPath getIndexPathForItemAtPoint(const Point &point) const;
        //- (NSIndexPath *)indexPathForCell:(PSTCollectionViewCell *)cell;
        CollectionViewCell *getCellForItemAtIndexPath(const IndexPath &indexPath);
        //- (NSArray *)visibleCells;
        //- (NSArray *)indexPathsForVisibleItems;

        // Interacting with the collection view.
        void scrollToItemAtIndexPath(const IndexPath &indexPath, CollectionViewScrollPosition scrollPosition, bool animated);
        
        // These methods allow dynamic modification of the current set of items in the collection view
        //- (void)insertSections:(NSIndexSet *)sections;
        //- (void)deleteSections:(NSIndexSet *)sections;
        //- (void)reloadSections:(NSIndexSet *)sections;
        //- (void)moveSection:(NSInteger)section toSection:(NSInteger)newSection;
        //- (void)insertItemsAtIndexPaths:(NSArray *)indexPaths;
        //- (void)deleteItemsAtIndexPaths:(NSArray *)indexPaths;
        //- (void)reloadItemsAtIndexPaths:(NSArray *)indexPaths;
        //- (void)moveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)newIndexPath;
        //- (void)performBatchUpdates:(void (^)(void))updates completion:(void (^)(BOOL finished))completion; // allows multiple insert/delete/reload/move calls to be animated simultaneously. Nestable.
        
        
        // Overrides
        virtual bool onTouchBegan(Touch *touch, Event *unusedEvent) override;
        virtual void onTouchMoved(Touch *touch, Event *unusedEvent) override;
        virtual void onTouchEnded(Touch *touch, Event *unusedEvent) override;
        virtual void onTouchCancelled(Touch *touch, Event *unusedEvent) override;
        
        virtual void update(float dt) override;
        
        void updateVisibleCells();
        
        void selectItem(const IndexPath &indexPath, CollectionViewScrollPosition scrollPosition, bool animated);
        void deselectItem(const IndexPath &indexPath, bool animated);
        
        virtual void startAttenuatingAutoScroll(const Vec2& deltaMove, const Vec2& initialVelocity) override;
        
    protected:
        virtual void onSizeChanged() override;
        virtual void doLayout()override;
        virtual void handleReleaseLogic(Touch *touch) override;
        
    private:
        int getPageNumberForContentOffset(const Vec2 offset) const;
        void disableTouchesRecursively(cocos2d::ui::Widget *widget);
        
        void invalidateLayout();
        //void updateVisibleCells();
        cocos2d::Rect getVisibleBounds() const;
        
        CollectionViewCell *createPreparedCell(const IndexPath &indexPath, const CollectionViewLayoutAttributes &attributes);
        CollectionReusableView *createPreparedSupplementaryView(const std::string &elementKind, const IndexPath &indexPath, const CollectionViewLayoutAttributes &attributes);
        
        void reuseCell(CollectionViewCell *cell);
        void reuseSupplementaryView(CollectionReusableView *reusableView);
        
        void addControlledChild(CollectionReusableView *child);
        
        void highlightItem(const IndexPath &indexPath, bool animated, CollectionViewScrollPosition scrollPosition);
        void unhighlightItem(const IndexPath &indexPath, bool animated);
        
        void userSelectedItemAtIndexPath(const IndexPath &indexPath);
        void selectItem(const IndexPath &indexPath, CollectionViewScrollPosition scrollPosition, bool animated, bool notifyDelegate);
        void deselectItem(const IndexPath &indexPath, bool animated, bool notifyDelegate);
        
        Vec2 getScrollOffsetForItemWithFrame(const Rect frame, CollectionViewScrollPosition scrollPosition) const;
        
    private:
        //support for collection view layout
        friend class cocos2d::ui::CollectionViewLayout;
        CollectionViewUpdateInfo getCurrentUpdate() const { return _update; }
        RefPtr<CollectionViewData> getCollectionViewData() const { return _collectionViewData; }
        
    private:
        template <typename T>
        using ReuseQueues = std::unordered_map<std::string, cocos2d::Vector<T *>>;
        
        RefPtr<CollectionViewLayout> _layout;
        
        std::set<IndexPath> _indexPathsForSelectedItems;
        std::set<IndexPath> _indexPathsForHighlightedItems;
        ReuseQueues<CollectionViewCell> _cellReuseQueues;
        ReuseQueues<CollectionReusableView> _supplementaryViewReuseQueues;
        
        std::unordered_map<std::string, CollectionViewCellFactoryPtr> _cellTemplatesDict;
        std::unordered_map<std::string, CollectionReusableViewFactoryPtr> _supplementaryViewTemplatesDict;
        
        RefPtr<CollectionViewData> _collectionViewData;
        cocos2d::Map<CollectionViewItemKey, CollectionReusableView *> _allVisibleViewsDict;
        
        int _reloadingSuspendedCount = 0;

        CollectionViewUpdateInfo _update;
        
        IndexPath _touchingIndexPath;
        IndexPath _currentIndexPath;
        float _maxTouchMoveDistance = 0.0f;
        float _touchMoveThreshold = 20.0f;
        int _touchId = -1;
        bool _touchFailed = false;
        
    private:
        template <typename T>
        void queueReusableView(T *reusableView, ReuseQueues<T> &queues, const std::string &identifier) {
            reusableView->removeFromParent();
            reusableView->prepareForReuse();
            
            //enqueu
            auto it = queues.find(identifier);
            if (it == queues.end()) {
                cocos2d::Vector<T *> newQueue;
                newQueue.pushBack(reusableView);
                queues[identifier] = newQueue;
            } else {
                (*it).second.pushBack(reusableView);
            }
        }
    };
}

NS_CC_END