//
//  UIGridLayoutSection.cpp
//  NewCollectionView
//
//  Created by Mikhail Shulepov on 17.12.15.
//
//

#include "UIGridLayoutSection.hpp"
#include "UIGridLayoutItem.hpp"
#include "UIGridLayoutRow.hpp"
#include "UIGridLayoutInfo.hpp"

NS_CC_BEGIN
namespace ui {
    void GridLayoutSection::invalidate() {
        _isValid = false;
        _rows.clear();
    }
    
    void GridLayoutSection::computeLayout() {
        if (!_isValid) {
            const bool isHorizontal = _layoutInfo->getHorizontal();
            
            // iterate over all items, turning them into rows.
            Size sectionSize = Size::ZERO;
            Index rowIndex = 0;
            Index itemIndex = 0;
            ssize_t itemsByRowCount = 0;
            float dimensionLeft = 0;
            GridLayoutRow *row = nullptr;
            // get dimension and compensate for section margin
            const float headerFooterDimension = _layoutInfo->getDimension();
            float dimension = headerFooterDimension;
            
            if (isHorizontal) {
                dimension -= _sectionMargins.top + _sectionMargins.bottom;
                _headerFrame = Rect(sectionSize.width, 0, _headerDimension, headerFooterDimension);
                sectionSize.width = _headerDimension + _sectionMargins.left;
                sectionSize.height = _headerFrame.size.height;
            } else {
                dimension -= _sectionMargins.left + _sectionMargins.right;
                _headerFrame = Rect(0, sectionSize.height, headerFooterDimension, _headerDimension);
                sectionSize.height = _headerDimension + _sectionMargins.top;
                sectionSize.width = _headerFrame.size.width;
            }
            
            float spacing = isHorizontal ? _verticalInterstice : _horizontalInterstice;
            
            const ssize_t itemsCount = getItemsCount();
            do {
                const bool finishCycle = itemIndex >= itemsCount;
                // TODO: fast path could even remove row creation and just calculate on the fly
                GridLayoutItem *item = nullptr;
                if (!finishCycle) {
                    item = _fixedItemSize ? nullptr : _items.at(itemIndex);
                }
                
                const Size itemSize = _fixedItemSize
                    ? _itemSize
                    : (item ? item->getFrame().size : Size::ZERO);
                float itemDimension = isHorizontal ? itemSize.height : itemSize.width;
                // first item of each row does not add spacing
                if (itemsByRowCount > 0) {
                    itemDimension += spacing;
                }
                if (dimensionLeft < itemDimension || finishCycle) {
                    // finish current row
                    if (row) {
                        // compensate last row
                        _itemsByRowCount = std::max(_itemsByRowCount, itemsByRowCount);
                        row->setItemCount(itemsByRowCount);
                        
                        // if current row is done but there are still items left, increase the incomplete row counter
                        if (!finishCycle) {
                            _indexOfIncompleteRow = rowIndex;
                        }
                        
                        row->layoutRow();
                        
                        const Size rowSize = row->getRowSize();
                        if (isHorizontal) {
                            const Rect rowFrame(sectionSize.width, _sectionMargins.top, rowSize.width, rowSize.height);
                            row->setRowFrame(rowFrame);
                            sectionSize.height = std::max(sectionSize.height, rowSize.height + rowFrame.origin.y);
                            sectionSize.width += rowSize.width + (finishCycle ? 0 : _horizontalInterstice);
                        } else {
                            const Rect rowFrame(_sectionMargins.left, sectionSize.height, rowSize.width, rowSize.height);
                            row->setRowFrame(rowFrame);
                            sectionSize.height += rowSize.height + (finishCycle ? 0 : _verticalInterstice);
                            sectionSize.width = std::max(rowSize.width + rowFrame.origin.x, sectionSize.width);
                        }
                    }
                    
                    // add new rows until the section is fully laid out
                    if (!finishCycle) {
                        // create new row
                        if (row) {
                            row->setComplete(true); // finish up current row
                        }
                        row = addRow();
                        row->setFixedItemSize(_fixedItemSize);
                        row->setIndex(rowIndex);
                        _indexOfIncompleteRow = rowIndex;
                        rowIndex++;
                        // convert an item from previous row to current, remove spacing for first item
                        if (itemsByRowCount > 0) {
                            itemDimension -= spacing;
                        }
                        dimensionLeft = dimension - itemDimension;
                        itemsByRowCount = 0;
                    }
                    
                } else {
                    dimensionLeft -= itemDimension;
                }
                
                // add item on slow path
                if (item) {
                    row->addItem(item);
                }
                
                itemIndex++;
                itemsByRowCount++;
            } while (itemIndex <= itemsCount);  // cycle once more to finish last row
            
            if (isHorizontal) {
                sectionSize.width += _sectionMargins.right;
                _footerFrame = Rect(sectionSize.width, 0, _footerDimension, headerFooterDimension);
                sectionSize.width += _footerDimension;
            } else {
                sectionSize.height += _sectionMargins.bottom;
                _footerFrame = Rect(0, sectionSize.height, headerFooterDimension, _footerDimension);
                sectionSize.height += _footerDimension;
            }
            
            _frame = Rect(Point::ZERO, sectionSize);
            _isValid = true;
        }
    }
    
    void GridLayoutSection::recomputeFromIndex(Index index) {
        //TODO: use index
        invalidate();
        computeLayout();
    }
    
    GridLayoutItem *GridLayoutSection::addItem() {
        GridLayoutItem *item = new GridLayoutItem;
        item->setSection(this);
        _items.pushBack(item);
        item->autorelease();
        return item;
    }
    
    GridLayoutRow *GridLayoutSection::addRow() {
        GridLayoutRow *row = new GridLayoutRow;
        row->setSection(this);
        _rows.pushBack(row);
        row->autorelease();
        return row;
    }
    
    GridLayoutSection *GridLayoutSection::snapshot() const {
        GridLayoutSection *copy = new GridLayoutSection;
        copy->_items = _items;
        copy->_rows = _rows;
        copy->setVerticalInterstice(_verticalInterstice);
        copy->setHorizontalInterstice(_horizontalInterstice);
        copy->setSectionMargins(_sectionMargins);
        copy->setFrame(_frame);
        copy->setHeaderFrame(_headerFrame);
        copy->setFooterFrame(_footerFrame);
        copy->setHeaderDimension(_headerDimension);
        copy->setFooterDimension(_footerDimension);
        copy->setLayoutInfo(_layoutInfo);
        copy->setAlignmentOptions(_alignmentOptions);
        copy->setFixedItemSize(_fixedItemSize);
        copy->setItemSize(_itemSize);
        copy->setItemsCount(_itemsCount);
        //snapshotSection.otherMargin = self.otherMargin;
        //snapshotSection.beginMargin = self.beginMargin;
        //snapshotSection.endMargin = self.endMargin;
        //snapshotSection.actualGap = self.actualGap;
        //snapshotSection.lastRowBeginMargin = self.lastRowBeginMargin;
        //snapshotSection.lastRowEndMargin = self.lastRowEndMargin;
        //snapshotSection.lastRowActualGap = self.lastRowActualGap;
        //snapshotSection.lastRowIncomplete = self.lastRowIncomplete;
        copy->_itemsByRowCount = _itemsByRowCount;
        copy->_indexOfIncompleteRow = _indexOfIncompleteRow;
        copy->autorelease();
        return copy;
    }
    
}
NS_CC_END